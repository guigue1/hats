import os
from pathlib import Path
import xml.etree.ElementTree as xmlet
import numpy as np
from astropy.io import fits
import collections
import pdb

__version__       = "2021-11-24T1900BRT"
__SIGNAL_LENGTH__ =  8192
__DATA_FILE__     = "hats_data_rbd.bin"
__HUSEC_FILE__    = "hats_husec.bin"

##############################################
#
# HATS: A python class to read and deconvolve HATS RBD data.
#
# Usage:
#   > export HATSXMLTABLES=/my/directory/with/HATS/XMLTABLES
#   >>> import HATS
#   >>> h=HATS.rbd()                               # Create the Object. Data is supposed to be in the same directory.
#   >>> h.from_file('hats-2021-08-26T1800.rbd')    # Import data from RBD file.
#   >>> h.getFFT()                                 # Deconvolve the data.
#
# Data Structures:
#   MetaData:   Dictionary with
#                               InputPath : string with directory where files are
#                               PathToXML : string with XML schema files
#                               File_Name : string with the RBD name file
#                               Date      : ISO data date
#                               Hour      : Data Hour (UTC)
#                               FFTProgram: full path to FFT module (C)
#  rData: A numpy array with raw data
#                               sample    : sample number (np.uint32 )
#                               sec       : seconds from 1970-01-01, Unix time (np.uint32)
#                               ms        : milliseconds of the seconds (np.uint16)
#                               husec     : hundred of microsends since 0 UT, as in SST (np.uint64)
#                               golay     : Golay output in ADC units (np.uint32)
#                               chopper   : Power Supply in ADC units (np.uint32)
#                               temp_env
#                               temp_hics
#                               temp_golay
#
# cData: A numpy array with converted data
#                               golay     : Golay output in ADC units (np.uint32)
#                               chopper   : Power Supply in ADC units (np.uint32)
#                               temp_env
#                               temp_hics
#                               temp_golay
#
# Deconv: a Dictionary with
#                               amplitude : FFT amplitude in ADC units (np.float64)
#                               husec     : time of 'amplitude' in hundred of microsends since 0 UT, as in SST (np.uint64)
#                               time      : time of 'amplitude' in datetime()
#
# Author: @guiguesp - São Paulo - returning to presencial classes - 2021-10-12
#                     Added more descriptions in XML file
#                     Simplified the readout of the Analog Devices variables
#                     Added the 'add' functionality
#
############################################################################

class aux(object):
    
    def __init__(self,PathToXML='',InputPath='./'):

        # PathToXML should point to tha directory where the XML tables are copied
        # When not defined, look at the environment
        if (isinstance(PathToXML,str) and len(PathToXML) == 0):
            if ('HATSXMLPATH' in os.environ.keys()):
                PathToXML=os.environ['HATSXMLPATH']
            else:
                PathToXML='.'
        else:
            PathToXML=PathToXML

        if PathToXML[-1] != '/' :
            PathToXML=PathToXML+'/'
        
        if (os.path.exists(PathToXML)):
            self.MetaData = {"XMLAuxDescriptionFile":PathToXML+"HATSAuxFormat.xml"}
        else:
            print("\n\n No XML directory {0:s} found \nExit\n\n".format(PathToXML))
            return            

        if (os.path.exists(InputPath)):
            self.MetaData.update({'InputPath':InputPath})
            self.Data     = np.empty((0))                        
        else:
            print("\n\n No data directory {0:s} found \nExit\n\n".format(InputPath))
            return            

    def __str__(self):
        return 'A Class representing HATS Auxiliary Data'

    def version(self):
        self.version = __version__
        return self.version
    
    def from_file(self, fname):

        fullpathname = self.MetaData['InputPath']+'/'+fname
        if (not os.path.exists(str(fullpathname))):
            print('\n\n File {0:s} not found \n\n'.format(fullpathname))
            return

        self.MetaData.update({'File_Name':fname})
        if ( (self.MetaData['File_Name'][:4] != "hats") & (self.MetaData['File_Name'][-4:] != "rbd") ) :
            raise ValueError("Invalid file type {}".format(self.filename))
        else:
            self.MetaData.update({'Date':fname[5:15]})
            self.MetaData.update({'Hour':fname[16:18]})

        if (os.path.exists(self.MetaData['XMLAuxDescriptionFile'])):            
            xml = xmlet.parse(self.MetaData['XMLAuxDescriptionFile']).getroot()
        else:
            print("\n\n No XML Auxiliary Data Description file {0:s} found \nExit\n\n".format(self.MetaData['XMLAuxDescriptionFile']))
            return            

        self._tblheader = dict()
        self._tblheader = collections.OrderedDict()

        for child in xml:
            var_name = child[0].text
            var_dim  = int(child[1].text)
            var_type = child[2].text
            var_unit = child[3].text

            if var_type == "xs:int":
                np_type = np.int32
            if var_type == "xs:unsignedInt":
                np_type = np.uint32                
            if var_type == "xs:unsignedShort":
                np_type = np.uint16            
            if var_type == "xs:unsignedLong":
                np_type = np.uint64
            if var_type == "xs:double":
                np_type = np.float64

            self._tblheader.update({var_name:[var_dim, np_type, var_unit]})

        dt_list = list()
        for key, value in self._tblheader.items():
            dt_list.append((key, value[1], value[0]))
        
        fhandler = open(fullpathname,'rb')
        self.Data = np.fromfile(fhandler, dt_list)     # raw Data, no calibration
                                   
        return

    def husec2dt(self,husec):
        import datetime as dt

        ms           = husec
        hours        = int(ms // 36000000)
        minutes      = int((ms % 36000000) // 600000)
        seconds      = ((ms % 36000000) % 600000) / 1.0E+04
        seconds_int  = int(seconds)
        seconds_frac = seconds - int(seconds)
        useconds     = int(seconds_frac * 1e6)
        year         = int(self.MetaData['Date'][0:4])
        month        = int(self.MetaData['Date'][5:7])
        day          = int(self.MetaData['Date'][8:10])
        return dt.datetime(year,month,day,hours,minutes,seconds_int,useconds)

    def getTimeAxis(self):
        """

        getTimeAxis: Class method to convert the us time axis used in RBD files to a Python
                     datetime ndarray that can be used with matplotlib.pyplot.

        Change Record:  First written by Guigue @ Sampa
                        2017-11-04 St Charles Day !!!

        """

        dt_time = list(map(self.husec2dt,self.rData['husec']))
        return np.asarray(dt_time)
    
class rbd(object):

    def __init__(self,PathToXML='',InputPath='./'):

        # PathToXML should point to tha directory where the XML tables are copied
        # When not defined, look at the environment
        if (isinstance(PathToXML,str) and len(PathToXML) == 0):
            if ('HATSXMLPATH' in os.environ.keys()):
                PathToXML=os.environ['HATSXMLPATH']
            else:
                PathToXML='.'
        else:
            PathToXML=PathToXML

        if PathToXML[-1] != '/' :
            PathToXML=PathToXML+'/'
        
        if (os.path.exists(PathToXML)):
            self.MetaData = {"XMLDataDescriptionFile":PathToXML+"HATSDataFormat.xml"}
        else:
            print("\n\n No XML directory {0:s} found \nExit\n\n".format(PathToXML))
            return            


        if (os.path.exists(InputPath)):
            self.MetaData.update({'InputPath':InputPath})
            self.Data     = np.empty((0))                        
        else:
            print("\n\n No data directory {0:s} found \nExit\n\n".format(InputPath))
            return            

    def __str__(self):
        return 'A Class representing HATS Raw Binary Data'

    def version(self):
        self.version = __version__
        return self.version
    
    def from_file(self, fname):

        fullpathname = self.MetaData['InputPath']+'/'+fname
        if (not os.path.exists(str(fullpathname))):
            print('\n\n File {0:s} not found \n\n'.format(fullpathname))
            return

        self.MetaData.update({'File_Name':fname})
        if ( (self.MetaData['File_Name'][:4] != "hats") & (self.MetaData['File_Name'][-4:] != "rbd") ) :
            raise ValueError("Invalid file type {}".format(self.filename))
        else:
            self.MetaData.update({'Date':fname[5:15]})
            self.MetaData.update({'Hour':fname[16:18]})

        if (os.path.exists(self.MetaData['XMLDataDescriptionFile'])):            
            xml = xmlet.parse(self.MetaData['XMLDataDescriptionFile']).getroot()
        else:
            print("\n\n No XML Data Description file {0:s} found \nExit\n\n".format(self.MetaData['XMLDataDescriptionFile']))
            return            

        self.MetaData.update({'FFTProgram':xml.attrib['FFTProgram']})
        
        self._tblheader = dict()
        self._tblheader = collections.OrderedDict()

        for child in xml:
            var_name = child[0].text
            var_dim  = int(child[1].text)
            var_type = child[2].text
            var_unit = child[3].text
            var_orig = child[4].text
            var_conv = child[5].text
            var_ord  = float(child[6].text)
            var_slop = float(child[7].text)

            if var_type == "xs:int":
                np_type = np.int32
            if var_type == "xs:unsignedInt":
                np_type = np.uint32                
            if var_type == "xs:unsignedShort":
                np_type = np.uint16            
            if var_type == "xs:unsignedLong":
                np_type = np.uint64

            self._tblheader.update({var_name:[var_dim, np_type, var_unit,var_orig,var_conv,var_ord,var_slop]})

        dt_list = list()
        for key, value in self._tblheader.items():
            dt_list.append((key, value[1], value[0]))
        
        fhandler = open(fullpathname,'rb')
        self.rData = np.fromfile(fhandler, dt_list)     # raw Data, no calibration

        # Number convertion for data coming from the Analog Devices A/D board.
        # They come in a 4 bytes integer, however, only the last 3 are significant.
        # The sign is in the third byte, the first bit.
        for key,val in self._tblheader.items():
            if (val[3] == "ad7770"):
                adcu = self.rData[key]&0x00FFFFFF             # Get the last 2 bytes
                negative = (self.rData[key]&0x0800000)>0      # Get the sign bit
                adcu[negative] = adcu[negative]-0x1000000    # Subtract the 'excess' to have negative numbers.
                self.rData[key] = adcu

        # Calibrated Data. 
        self.cData = {}
        for key,val in self._tblheader.items():
            if (val[4] == "yes"):                                            # Data must be calibrated
                self.cData.update({key:np.zeros(self.rData[key].shape[0])})  # Create room for the data
                self.cData[key] = self.rData[key] * val[6] + val[5]          # apply a linear conversion

                                   
        return

    def husec2dt(self,husec):
        import datetime as dt

        ms           = husec
        hours        = int(ms // 36000000)
        minutes      = int((ms % 36000000) // 600000)
        seconds      = ((ms % 36000000) % 600000) / 1.0E+04
        seconds_int  = int(seconds)
        seconds_frac = seconds - int(seconds)
        useconds     = int(seconds_frac * 1e6)
        year         = int(self.MetaData['Date'][0:4])
        month        = int(self.MetaData['Date'][5:7])
        day          = int(self.MetaData['Date'][8:10])
        return dt.datetime(year,month,day,hours,minutes,seconds_int,useconds)

    def getTimeAxis(self):
        """

        getTimeAxis: Class method to convert the us time axis used in RBD files to a Python
                     datetime ndarray that can be used with matplotlib.pyplot.

        Change Record:  First written by Guigue @ Sampa
                        2017-11-04 St Charles Day !!!

        """

        dt_time = list(map(self.husec2dt,self.rData['husec']))
        return np.asarray(dt_time)

    def getFFT(self):

        Nchunks = self.rData['husec'].shape[0] // __SIGNAL_LENGTH__
        Lchunk = self.rData['husec'].shape[0] % __SIGNAL_LENGTH__
        amp = []
        hus = []
        
        self.rData['husec'].tofile(__HUSEC_FILE__)
        self.rData['golay'].tofile(__DATA_FILE__)

        if os.path.exists(self.MetaData['FFTProgram']):
            os.spawnl(os.P_WAIT,self.MetaData['FFTProgram'],os.path.basename(self.MetaData['FFTProgram']) )
        else:
            print("\n\n No {0:s}  module found in {1:s}\nExit\n\n".format(os.path.basename(self.MetaData['FFTProgram']),
                                                                          os.path.dirname(self.MetaData['FFTProgram'])))
            return            
        
        husec  = np.fromfile(__HUSEC_FILE__ , dtype=np.uint64  , count=-1)
        fftAmp = np.fromfile(__DATA_FILE__  , dtype=np.float64 , count=-1)

        os.remove(__HUSEC_FILE__)
        os.remove(__DATA_FILE__)
        dt_time = list(map(self.husec2dt,husec))
        self.Deconv = {'amplitude': fftAmp, 'husec':husec, 'time':np.asarray(dt_time)}

        return 

    def check(self):

        t_initial = self.husec2dt(self.rData['husec'][0])
        t_final   = self.husec2dt(self.rData['husec'][-1])

        ds        = self.rData['sample'][1:]-self.rData['sample'][:-1]
        leap_ds   = (ds != 1)
        list_sample_leaps = np.where(leap_ds)

        dhusec     = self.rData['husec'][1:]-self.rData['husec'][:-1]
        leap_husec = (dhusec != 10)
        list_husec_leaps = np.where(leap_husec)

        sec      = np.empty(self.rData['sec'].shape[0],dtype=np.float64)
        sec      = self.rData['sec']+self.rData['ms']/1000
        dsec     = sec[1:]-sec[:-1]
        leap_sec = (np.abs(dsec) < 0.0009) | (np.abs(dsec) > 0.00101) 
        list_sec_leaps = np.where(leap_sec)
        
        # Print Stats
        print("\n\n")
        print(" - - - STATS - - - \n\n")
        print("Total Records : {0:d}".format(self.rData['sample'].shape[0]))
        print("Initial Date  : {0:s} \nFinal Date    : {1:s} \nTotal Duration: {2:d} seconds \n\n".format(t_initial.strftime("%c"),
                                                                                         t_final.strftime("%c"),
                                                                                         (t_final-t_initial).seconds ))
        print("Mean Values")
        print("          Golay : {0:f} mV".format(self.cData['golay'].mean()))
        print("   Temp HICS    : {0:f} C".format(self.cData['temp_hics'].mean()))
        print("   Temp Ambient : {0:f} C".format(self.cData['temp_env'].mean()))
        print("   Temp Golay   : {0:f} C".format(self.cData['temp_golay'].mean()))
        print("\n")

        if (list_sample_leaps[0].shape[0] > 0):
            print("    Sample leaps > 1\n")
            i=1
            for y in list_sample_leaps:
                for x in y:
                    init = self.rData['sample'][x]
                    final = self.rData['sample'][x+1]
                    if (final >= init):
                        leap = final-init
                    else:
                        leap = -int(init-final)
                        
                    print("      # : {0:d}    Index: {1:6d}  Init = {2:6d} Next = {3:6d} Leap = {4:6d}".format(i, x,init,final,leap))
                    i+=1

        if (list_husec_leaps[0].shape[0] > 0):
            i=1
            print("    Husec leaps > 10\n")
            for y in list_husec_leaps:
                for x in y:
                    init = self.rData['husec'][x]
                    final = self.rData['husec'][x+1]
                    if (final >= init):
                        leap = final-init
                    else:
                        leap = -int(init-final)
                    
                    print("      # : {0:d}    Index: {1:6d}  Init = {2:6d} husec Next = {3:d} husec , Leap = {4:f} seconds".format(i, x, init, final,leap*1e-4))
                    i+=1

        if (list_sec_leaps[0].shape[0] > 0):
            i=1
            print("    Sec leaps > 0.001\n")
            for y in list_sec_leaps:
                for x in y:
                    print("      # : {0:d}    Index: {1:6d}  , Leap = {2:f} seconds".format(i, x, sec[x+1]-sec[x]))
                    i+=1
                    
        print(" - - - - - - - - - - -  \n\n")
        return list_husec_leaps,list_sample_leaps
                                  
    def __add__(self,h):

        _temp_ = rbd()

        Date = [self.MetaData['Date'],h.MetaData['Date']]
        Hour = [self.MetaData['Hour'],h.MetaData['Hour']]
        FNam = [self.MetaData['File_Name'], h.MetaData['File_Name']]
        XML  = [self.MetaData['XMLDataDescriptionFile'], h.MetaData['XMLDataDescriptionFile']]
        FFT  = [self.MetaData['FFTProgram'], h.MetaData['FFTProgram']]
        PATH = [self.MetaData['InputPath'], h.MetaData['InputPath']]
        
        _temp_.MetaData = {'XMLDataDescriptionFile': XML,
                           'InputPath': PATH,
                           'File_Name': FNam,
                           'Date': Date,
                           'Hour': Hour,
                           'FFTProgram': FFT}
                                

        _temp_.rData = np.concatenate((self.rData,h.rData))
        
        _temp_.cData     = {}        
        TagList = list(self.cData.keys())
        for iTag in TagList:
            _temp_.cData[iTag] = np.concatenate((self.cData[iTag],h.cData[iTag]))
        
        if (hasattr(self,'Deconv') & hasattr(h,'Deconv')):
            _temp_.Deconv = {}
            for iTag in TagList:
                _temp_.Deconv[iTag] = np.concatenate((self.Deconv[iTag],h.Deconv[iTag]))
            
        return _temp_
    
