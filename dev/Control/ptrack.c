/*
	==============================================================
	Universidade Presbiteriana Mackenzie
	Centro de Rádio Astronomia e Astrofísica Mackenzie - CRAAM
	==============================================================

	Point and Track
	---------------------------------------------------
	Este programa visa realizar o apontamento para o objeto (slew) e
	posteriormente realizar o acompanhamento (track) utilizando as
	definicoes de track (velocidade) de acordo com o tracking rate
	fornecido pelo TheSkyX do objeto.

	Utilizadas classes:
	sky6RASCOMTele e sky6ObjectInformation
	---------------------------------------------------

	Autor: Tiago Giorgetti
	Email: tiago.giorgetti@craam.mackenzie.br

	Histórico:
	_______________________________________________________________________________
	 Versão	|  Data		|	Atualização
	-------------------------------------------------------------------------------
	  1.0	|  19-11-2019	| Modificação no javascript para coleta de todos os 
		|		| dados de uma unica vez.
	-------------------------------------------------------------------------------

	Uso:

	# ptrack <object>

	Objetos disponíveis: Sun, Moon, Saturn, Jupiter


*/

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <termio.h>
#include <unistd.h>

#include <sys/socket.h> //socket
#include <arpa/inet.h>  //inet_addr
#include <fcntl.h>      //open(sock)
#include <unistd.h>     //close(sock)



//Parametros de conexao
#define IP_SERVER "10.0.92.12"
#define TCP_PORT 3040
#define RCV_BUFFER_SIZE 2000


bool kbhit(void)
{
	struct termios original;
	tcgetattr(STDIN_FILENO, &original);
	struct termios term;
	memcpy(&term, &original, sizeof(term));
	term.c_lflag &= ~ICANON;
	tcsetattr(STDIN_FILENO, TCSANOW, &term);
	int characters_buffered = 0;
	ioctl(STDIN_FILENO, FIONREAD, &characters_buffered);
	tcsetattr(STDIN_FILENO, TCSANOW, &original);
	bool pressed = (characters_buffered != 0);
	return pressed;
}

/* Move string a para b */
void copy_char ( char a[], char b[])
{
	int i = 0;
	do
	{
        	b[i] = a[i];
                i++;
        }while ( a[i] != '|' );
}

/* Conta quantos elementos do vetor de vetores  */

int cont_vector_lines ( char* a[])
{
	int i = 0;
	while ( a[i] != "!")
	{
		i++;
	}
	return i;
}


int string_token( char mesg[] )
{
//	char mesg[40] = " 123 | 456 | -789 | nada nada nada ";
	char * p;
	const char sep[2] ="|";
	double d,m,s;

	fprintf(stderr,"\n\n Message = %s\n\n",mesg);
	p = strtok(mesg,sep);
	if ( p != NULL)
	{
		fprintf(stderr,"\n 1: %s\n",p);
		d= atof(p);
		p= strtok(NULL,sep);
	}


	if ( p != NULL)
	{
		fprintf(stderr,"\n 2: %s\n",p);
		m = atof(p);
		p = strtok(NULL,sep);
	}


	if ( p != NULL)
	{
		fprintf(stderr,"\n 2: %s\n",p);
		s= atof(p);
	}

	fprintf(stderr,"\n\n Number = %5.3f:%5.3f:%5.3f\n\n",d,m,s);

	return 0;
}





int main(int argc , char *argv[])
{


/*        struct pos_data_type
        {
                unsigned long long time_Husec   ;       //Time: Husec
                double time_JD                  ;       //Time: Julian Date
                double time_Sid                 ;       //Time: Sideral
                //--------------------------------------------------------
                double pos_tele_alt             ;       //Telescope Position: Altitude
                double pos_tele_az              ;       //Telescope Position: Azimute
                double pos_tele_ra              ;       //Telescope Position: Right Ascension
                double pos_tele_dec             ;       //Telescope Position: Declination
                //---------------------------------------------------------
                double rate_ObjId_ra            ;       //Tracking rate: Right Ascension diff from sidereal rate
                double rate_ObjId_dec           ;       //Tracking rate: Declination diff from sidereal rate

        } hats_pos_ringbuffer[RINGSIZE], *rb_ptr ;
*/


	long int j = 0;
	int k = 0;
	const char spin[4]={'|', '/', '-', '\\'};
	int i, sock;
	struct sockaddr_in server;
	char server_reply[RCV_BUFFER_SIZE];
	char position_data[RCV_BUFFER_SIZE];

	//----------Begin--Of--Javascripts--------------------------------------------

	char* get_data[] =
	{
		"/* Java Script */",
		"/* Socket Start Packet */",
		"var Out;",
		"sky6RASCOMTele.GetAzAlt();",
		"var alt = sky6RASCOMTele.dAlt;",
		"var az = sky6RASCOMTele.dAz;",
		"sky6RASCOMTele.GetRaDec();",
		"var ra = sky6RASCOMTele.dRa;",
                "var dec = sky6RASCOMTele.dDec;",
                "var tra = sky6RASCOMTele.dRaTrackingRate;",
                "var tdec = sky6RASCOMTele.dDecTrackingRate;",
                "sky6ObjectInformation.Property(173);",
                "var sidereal = sky6ObjectInformation.ObjInfoPropOut;",
                "sky6ObjectInformation.Property(174);",
                "var jd = sky6ObjectInformation.ObjInfoPropOut;",
		"Out = jd + ';' + sidereal + ';' + alt + ';' + az + ';' + ra + ';' + dec + ';' + tra + ';' + tdec + ';'",
		"/* Socket End Packet */",
		"!"	//End Caracter
	};


	//----------End--Of--Javascripts--------------------------------------------


	while(!kbhit())
	{

		//Create socket
		sock = socket(AF_INET , SOCK_STREAM , 0);
		if (sock == -1)
		{
			printf("Could not create socket");
		}
		//printf("Socket created\n");

		server.sin_addr.s_addr = inet_addr(IP_SERVER);
        	server.sin_family = AF_INET;
		server.sin_port = htons(TCP_PORT);

		//printf("ate qui!\n");

		//Connect to remote server
		if (connect(sock , (struct sockaddr *)&server , sizeof(server)) < 0)
		{
			printf("Connect failed. Error\n");
			return 1;
		}
		//printf("Connected\n");


		//------------------------------------------------------------------
		//------------ Bloco de GET para Posição Alt_Az --------------------

		// Send get data
		for(i = 0; i<cont_vector_lines(get_data) ; i++)
		{
			if( send(sock , get_data[i] , strlen(get_data[i]) , 0) < 0)
			{
				puts("Send failed. Error");
				return 1;
			}
		}
		//printf("Message sent\n");

		//Receive a reply from the server
		if( recv(sock , server_reply , RCV_BUFFER_SIZE , 0) < 0)
		{
			printf("Recv failed. Error\n");
		}

		//Copy char ( source , destiny )
//		copy_char ( server_reply , position_data );  // Util para remover a partir do "|" da resposta do servidor



                //------------------------------------------------------------------
                //------------ String Token Separation -----------------------------


//      	char mesg[40] = " 123 | 456 | -789 | nada nada nada ";
	        char * p;
        	const char sep[2] =";";
	        double jd , sid , alt, az , ra , dec , tra , tdec;

		system("clear"); // Limpar a tela

	        fprintf(stderr,"\r Message = %s\n\n",server_reply);
	        p = strtok(server_reply,sep);
	        if ( p != NULL)
	        {
	                fprintf(stderr,"\r jd: %s\n",p);
	                jd= atof(p);
	                p= strtok(NULL,sep);
	        }

	        if ( p != NULL)
        	{
	                fprintf(stderr,"\r sid: %s\n",p);
	                sid = atof(p);
        	        p = strtok(NULL,sep);
	        }

                if ( p != NULL)
                {
                        fprintf(stderr,"\r alt: %s\n",p);
                        alt = atof(p);
                        p = strtok(NULL,sep);
                }

                if ( p != NULL)
                {
                        fprintf(stderr,"\r az: %s\n",p);
                        az = atof(p);
                        p = strtok(NULL,sep);
                }

                if ( p != NULL)
                {
                        fprintf(stderr,"\r ra: %s\n",p);
                        ra = atof(p);
                        p = strtok(NULL,sep);
                }

                if ( p != NULL)
                {
                        fprintf(stderr,"\r dec: %s\n",p);
                        dec = atof(p);
                        p = strtok(NULL,sep);
                }

                if ( p != NULL)
                {
                        fprintf(stderr,"\r tra: %s\n",p);
                        tra = atof(p);
                        p = strtok(NULL,sep);
                }

        	if ( p != NULL)
	        {
        	        fprintf(stderr,"\r tdec: %s\n",p);
	                tdec = atof(p);
        	}

//	        fprintf(stderr,"\r Number = %5.3f:%5.3f:%5.3f\n\n",d,m,s);



                //------------------------------------------------------------------
		//----------- Bloco para exibicao na tela --------------------------


//		system("clear"); // Limpar a tela
		printf("Telescope Coordinates:\n");
		printf("-------------------------\n");
		printf("  jd     sidereal    Alt     Az      RA      Dec      RArate       DecRate\n");
		printf("-------------------------\n");
		puts(server_reply);
//		puts(position_data);
		printf("-------------------------\n");


		printf("\rLoop #%d\n", j);
		printf("\rLoading %c\n", spin[k]);
		fflush(stdout);
		j++;
		k++;
		if (k == 4) { k = 0; };
		close(sock);

	}

	printf("\n");
	return 0;
}

