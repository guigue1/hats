#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <semaphore.h>
#include <string.h>
#include <time.h>
#include "RingBuffer.h"


void delay(int number_of_seconds) 
{ 
    // Converting time into milli_seconds 
    int milli_seconds = 1000 * number_of_seconds; 
  
    // Stroing start time 
    clock_t start_time = clock(); 
  
    // looping till required time is not acheived 
    while (clock() < start_time + milli_seconds) 
        ; 
}

void report_and_exit(const char* msg) {
  perror(msg);
  exit(-1);
}

void main(){

  HatsData * ptr_HatsRecordBase, * ptr_HatsRecord ; 
  size_t ByteSize = sizeof(HatsData) * NREC ;
  int counter=0, fd;
  sem_t * semptr;
  
  fd = shm_open(BackingFile,      /* name from smem.h */
		O_RDWR | O_CREAT, /* read/write, create if needed */
		AccessPerms);     /* access permissions (0644) */
  if (fd < 0) report_and_exit("Can't open shared mem segment...");

  ftruncate(fd, ByteSize); /* get the bytes */

  ptr_HatsRecordBase = mmap(NULL,       /* let system pick where to put segment */
			    ByteSize,   /* how many bytes */
			    PROT_READ | PROT_WRITE, /* access protections */
			    MAP_SHARED, /* mapping visible to other processes */
			    fd,         /* file descriptor */
			    0);         /* offset: start at 1st byte */
  
  if ( (void *) -1  == ptr_HatsRecordBase)
    report_and_exit("Can't get segment...");
  else
    ptr_HatsRecord = ptr_HatsRecordBase;

  fprintf(stderr, "shared mem address: %p [0..%d]\n", ptr_HatsRecordBase, ByteSize - 1);
  fprintf(stderr, "backing file:       /dev/shm%s\n", BackingFile );

  /* semaphore code to lock the shared mem */
  semptr = sem_open(SemaphoreName, /* name */
		    O_CREAT,       /* create the semaphore */
		    AccessPerms,   /* protection perms */
		    0);            /* initial value */
  if (semptr == (void*) -1) report_and_exit("sem_open");

  while(1) {
    ptr_HatsRecord->azi  = (float) (rand() % 10) ;
    ptr_HatsRecord->ele  = (float) (rand() % 20) ;
    ptr_HatsRecord->time = (unsigned long long) (rand() % 50) ;
    
    {
      short i;
      for (i==0; i<= 4; i++) ptr_HatsRecord->adcu[i] = (int) (rand() % 5);
    }

    /* increment the semaphore so that memreader can read */
     if (sem_post(semptr) < 0) report_and_exit("sem_post");

    printf("Counter = %6i : Time = %5d , Azi = %5.2f  , Ele = %5.2f  ADCU = %d3.3, %d3.3, %d3.3, %d3.3, %d3.3\n",
	   counter, ptr_HatsRecord->time,
	   ptr_HatsRecord->azi,
	   ptr_HatsRecord->ele,
	   ptr_HatsRecord->adcu[0],
	   ptr_HatsRecord->adcu[1],
	   ptr_HatsRecord->adcu[2],
	   ptr_HatsRecord->adcu[3],
	   ptr_HatsRecord->adcu[4]);
    
    counter++;
    
    if (counter % NREC)
      {ptr_HatsRecord++;  }
    else
      {
	char v;
	printf("\n\n");
	//(void) read(0, &v, 1);
	delay(1);
	ptr_HatsRecord=ptr_HatsRecordBase;
      }

  }

}

    
