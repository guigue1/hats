#ifndef SHMEM
#define SHMEM

#define BackingFile "example"
#define ByteSize 512
#define SemaphoreName "semaphore"
#define AccessPerms 0644

#endif
