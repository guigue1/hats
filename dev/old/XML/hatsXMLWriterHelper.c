/**
 * Section: XMLHelpers
 * synopsis: Create / Update the Different XML files
 * author: Guigue (from the original code testWriter.c by Alfred Mickautsch)
 *         2019-11-11
 *********************************/

#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <time.h>
#include <unistd.h>
#include <libxml/encoding.h>
#include <libxml/xmlwriter.h>
#include <libxml/tree.h>
#include <libxml/parser.h>
#include <libxml/xpath.h>
#include <libxml/xpathInternals.h>

#include "hatsXMLwriter.h"
#include "hatsStructures.h"

#if defined(LIBXML_WRITER_ENABLED) && defined(LIBXML_OUTPUT_ENABLED)

xmlChar *ConvertInput(const char *in, const char *encoding);

void XMLWriter_Append_Table_Auxiliary_Observing_Modes(HatsTargetInfo target, char * xmlTableName)
{
  xmlTextWriterPtr writer;
  char tmp[100];
  xmlDocPtr doc;
  xmlXPathContextPtr xpathCtx; 
  xmlXPathObjectPtr xpathObj;
  xmlNodePtr rNode, newNode;
  xmlAttrPtr newAttr;
  
  LIBXML_TEST_VERSION

    printf(" XML filename = %s\n",xmlTableName);
  
    /*  Check whether the file exist or not */
  if ( (access(xmlTableName,F_OK | R_OK) == -1) ) {
      fprintf(stderr,"\n\n File Not Found \n\n");
      return;
  }
  
  fprintf(stderr,"\n\n File Found ! \n\n");
  /* Load XML document */
  if ((doc = xmlReadFile(xmlTableName,
			 NULL,
			 XML_PARSE_NOBLANKS | XML_PARSE_NOERROR | XML_PARSE_NOWARNING | XML_PARSE_NONET)) == NULL) {
    fprintf(stderr, "Error: unable to parse file \"%s\"\n", xmlTableName);
    errno = ECANCELED;
    return;
  }
  
  newNode = xmlNewNode (NULL,"TARGET");
  xmlNewChild(newNode, NULL, BAD_CAST "TELESCOPE_NAME", ConvertInput(target.tname, MY_ENCODING) )  ;
  xmlNewChild(newNode, NULL, BAD_CAST "COMMANDED_NAME", ConvertInput(target.cname, MY_ENCODING) )  ;
  xmlNewChild(newNode, NULL, BAD_CAST "RISE_TIME", ConvertInput(target.rise_time, MY_ENCODING) )  ;
  xmlNewChild(newNode, NULL, BAD_CAST "SET_TIME", ConvertInput(target.set_time, MY_ENCODING) )  ;
  xmlNewChild(newNode, NULL, BAD_CAST "TRANSIT_TIME", ConvertInput(target.transit_time, MY_ENCODING) )  ;
  sprintf(tmp,"%9.4f",target.azimuth_rise);
  xmlNewChild(newNode, NULL, BAD_CAST "AZIMUTH_RISE", ConvertInput(tmp, MY_ENCODING) )  ;
  sprintf(tmp,"%9.4f",target.azimuth_set);
  xmlNewChild(newNode, NULL, BAD_CAST "AZIMUTH_SET", ConvertInput(tmp, MY_ENCODING) )  ;
  sprintf(tmp,"%10.6f",target.sun_helio_long);
  xmlNewChild(newNode, NULL, BAD_CAST "SUN_HELIO_LONG", ConvertInput(tmp, MY_ENCODING) )  ;
  sprintf(tmp,"%10.6f",target.sun_helio_lat);
  xmlNewChild(newNode, NULL, BAD_CAST "SUN_HELIO_LAT", ConvertInput(tmp, MY_ENCODING) )  ;
  sprintf(tmp,"%10.6f",target.sun_pos_angle);
  xmlNewChild(newNode, NULL, BAD_CAST "SUN_POS_ANGLE", ConvertInput(tmp, MY_ENCODING) )  ;
  sprintf(tmp,"%9.6f",target.sun_dist_ly);
  xmlNewChild(newNode, NULL, BAD_CAST "SUN_DISTANCE_LY", ConvertInput(tmp, MY_ENCODING) )  ;
  xmlNewChild(newNode, NULL, BAD_CAST "OBSERVING_MODE", ConvertInput(target.observing_mode, MY_ENCODING) )  ;
  xmlNewChild(newNode, NULL, BAD_CAST "INITIAL_DATE", ConvertInput(target.initial_date, MY_ENCODING) )  ;
  sprintf(tmp,"%9d",target.initial_time);
  xmlNewChild(newNode, NULL, BAD_CAST "INITIAL_TIME", ConvertInput(tmp, MY_ENCODING) )  ;
  sprintf(tmp,"%14.6f",target.initial_jd);
  xmlNewChild(newNode, NULL, BAD_CAST "INITIAL_JD", ConvertInput(tmp, MY_ENCODING) )  ;
  xmlNewChild(newNode, NULL, BAD_CAST "FINAL_DATE", ConvertInput(target.final_date, MY_ENCODING) )  ;
  sprintf(tmp,"%9d",target.final_time);
  xmlNewChild(newNode, NULL, BAD_CAST "FINAL_TIME", ConvertInput(tmp, MY_ENCODING) )  ;
  sprintf(tmp,"%14.6f",target.final_jd);
  xmlNewChild(newNode, NULL, BAD_CAST "FINAL_JD", ConvertInput(tmp, MY_ENCODING) )  ;

  rNode = xmlDocGetRootElement(doc);
  xmlAddSibling(rNode, newNode);
  xmlSaveFormatFileEnc (xmlTableName, doc, MY_ENCODING,0);
  xmlFreeDoc(doc);

  return;
  
}

void XMLWriter_Create_Table_Auxiliary_Observing_Modes(HatsTargetInfo target, char * xmlTableName){

  int rc;
  xmlTextWriterPtr writer;
  xmlChar *tmp;

  LIBXML_TEST_VERSION
        
  /* Create a new XmlWriter for uri, with no compression. */
  if ((writer = xmlNewTextWriterFilename(xmlTableName, 0)) == NULL) {
    fprintf(stderr,"\nError creating the xml writer\n");
    errno = EACCES;
    return ;
  }

  /* Start the document with the xml default for the version,
   * encoding UTF-8 and the default for the standalone declaration. */
  if ( (rc = xmlTextWriterStartDocument(writer, NULL, MY_ENCODING, NULL)) < 0 ){
    fprintf(stderr,"\nError at xmlTextWriterStartDocument\n");
    return;
  }
  
  /***********************
   * Header Comment
   * ********************/
  tmp = ConvertInput("HATS XML Metadata File \nInformation about the object observed, including the observing mode. There are two separate information origins: the telescope (<telescope>)  and the control software (<commanded>).\n\nVersion: 0.9    (Preliminary )\n\nAuthor -   Guigue - 2019-11-01\n",MY_ENCODING);
      
  if ( (rc = xmlTextWriterWriteComment(writer, tmp)) < 0){
    fprintf(stderr,"\nError at xmlTextWriterWriteComment\n");
    errno= EACCES;
    return;
  }
  if (tmp != NULL) xmlFree(tmp);
  

  /****************************************
   * First Elements SSTDataFormat
   ****************************************/ 
  if ( (rc = xmlTextWriterStartElement(writer, BAD_CAST "TARGET")) < 0 ){
    fprintf(stderr,"\nError at xmlTextWriterStartElement\n");
    errno = EACCES;
    return;
  }
  
  if (  (rc = xmlTextWriterWriteElement(writer, BAD_CAST "TELESCOPE_NAME", ConvertInput(target.tname, MY_ENCODING))) < 0){
    fprintf(stderr,"testXmlwriterFilename: Error at xmlTextWriterWriteFormatElement\n");
    errno = EACCES;
    return;
  }
  
  if (  (rc = xmlTextWriterWriteElement(writer, BAD_CAST "COMMANDED_NAME", ConvertInput(target.cname, MY_ENCODING))) < 0){
    fprintf(stderr,"testXmlwriterFilename: Error at xmlTextWriterWriteFormatElement\n");
    errno = EACCES;
    return;
  }
  
  if (  (rc = xmlTextWriterWriteElement(writer, BAD_CAST "RISE_TIME", ConvertInput(target.rise_time, MY_ENCODING))) < 0){
    fprintf(stderr,"testXmlwriterFilename: Error at xmlTextWriterWriteFormatElement\n");
    errno = EACCES;
    return;
  }
  
  if (  (rc = xmlTextWriterWriteElement(writer, BAD_CAST "SET_TIME", ConvertInput(target.set_time, MY_ENCODING))) < 0){
    fprintf(stderr,"testXmlwriterFilename: Error at xmlTextWriterWriteFormatElement\n");
    errno = EACCES;
    return;
  }
  
  if (  (rc = xmlTextWriterWriteElement(writer, BAD_CAST "TRANSIT_TIME", ConvertInput(target.transit_time, MY_ENCODING))) < 0){
    fprintf(stderr,"testXmlwriterFilename: Error at xmlTextWriterWriteFormatElement\n");
    errno = EACCES;
    return;
  }
  
  if (  (rc = xmlTextWriterWriteFormatElement(writer, BAD_CAST "AZIMUTH_RISE", "%9.4f",target.azimuth_rise)) < 0){
    fprintf(stderr,"testXmlwriterFilename: Error at xmlTextWriterWriteFormatElement\n");
    errno = EACCES;
    return;
  }
  
  if (  (rc = xmlTextWriterWriteFormatElement(writer, BAD_CAST "AZIMUTH_SET", "%9.4f",target.azimuth_set)) < 0){
    fprintf(stderr,"testXmlwriterFilename: Error at xmlTextWriterWriteFormatElement\n");
    errno = EACCES;
    return;
  }
  
  if (  (rc = xmlTextWriterWriteFormatElement(writer, BAD_CAST "SUN_HELIO_LONG", "%10.6f",target.sun_helio_long)) < 0){
    fprintf(stderr,"testXmlwriterFilename: Error at xmlTextWriterWriteFormatElement\n");
    errno = EACCES;
    return;
  }
  
  if (  (rc = xmlTextWriterWriteFormatElement(writer, BAD_CAST "SUN_HELIO_LAT", "%10.6f",target.sun_helio_lat)) < 0){
    fprintf(stderr,"testXmlwriterFilename: Error at xmlTextWriterWriteFormatElement\n");
    errno = EACCES;
    return;
  }
  
  if (  (rc = xmlTextWriterWriteFormatElement(writer, BAD_CAST "SUN_POS_ANGLE", "%10.6f",target.sun_pos_angle)) < 0){
    fprintf(stderr,"testXmlwriterFilename: Error at xmlTextWriterWriteFormatElement\n");
    errno = EACCES;
    return;
  }
  
  if (  (rc = xmlTextWriterWriteFormatElement(writer, BAD_CAST "SUN_DISTANCE_LY", "%9.6f",target.sun_dist_ly)) < 0){
    fprintf(stderr,"testXmlwriterFilename: Error at xmlTextWriterWriteFormatElement\n");
    errno = EACCES;
    return;
  }
  
  if (  (rc = xmlTextWriterWriteElement(writer, BAD_CAST "OBSERVING_MODE", ConvertInput(target.observing_mode, MY_ENCODING))) < 0){
    fprintf(stderr,"testXmlwriterFilename: Error at xmlTextWriterWriteFormatElement\n");
    errno = EACCES;
    return;
  }
  
  if (  (rc = xmlTextWriterWriteElement(writer, BAD_CAST "INITIAL_DATE", ConvertInput(target.initial_date, MY_ENCODING))) < 0){
    fprintf(stderr,"testXmlwriterFilename: Error at xmlTextWriterWriteFormatElement\n");
    errno = EACCES;
    return;
  }
  
  if (  (rc = xmlTextWriterWriteFormatElement(writer, BAD_CAST "INITIAL_TIME", "%9d",target.initial_time)) < 0){
    fprintf(stderr,"testXmlwriterFilename: Error at xmlTextWriterWriteFormatElement\n");
    errno = EACCES;
    return;
  }
  
  if (  (rc = xmlTextWriterWriteFormatElement(writer, BAD_CAST "INITIAL_JD", "%14.6f",target.initial_jd) < 0) ){
    fprintf(stderr,"testXmlwriterFilename: Error at xmlTextWriterWriteFormatElement\n");
    errno = EACCES;
    return;
  }

  if (  (rc = xmlTextWriterWriteElement(writer, BAD_CAST "FINAL_DATE", ConvertInput(target.final_date, MY_ENCODING))) < 0){
    fprintf(stderr,"testXmlwriterFilename: Error at xmlTextWriterWriteFormatElement\n");
    errno = EACCES;
    return;
  }
  
  if (  (rc = xmlTextWriterWriteFormatElement(writer, BAD_CAST "FINAL_TIME", "%9d",target.final_time)) < 0){
    fprintf(stderr,"testXmlwriterFilename: Error at xmlTextWriterWriteFormatElement\n");
    errno = EACCES;
    return;
  }
  
  if (  (rc = xmlTextWriterWriteFormatElement(writer, BAD_CAST "FINAL_JD", "%14.6f",target.final_jd) < 0) ){
    fprintf(stderr,"testXmlwriterFilename: Error at xmlTextWriterWriteFormatElement\n");
    errno = EACCES;
    return;
  }
  
  if (  (rc = xmlTextWriterEndElement(writer)) < 0){
    fprintf(stderr,"testXmlwriterFilename: Error at xmlTextWriterEndElement\n");
    errno = EACCES;
    return;
  }

  if ( (rc = xmlTextWriterEndDocument(writer)) < 0 ){
    fprintf(stderr,"\nError at xmlTextWriterEndDocument\n");
    errno = EACCES;
    return;
  }
  
  xmlFreeTextWriter(writer);
  
  return;

}


/***************************************************************************************************************************************/

  
  
  
void XMLwriter_Header_Auxiliary_Position(char * XHFname, char * initial_date, char * initial_time)
{

  LIBXML_TEST_VERSION

  int rc;
  xmlTextWriterPtr writer;
  xmlChar *tmp;
  
  /* Create a new XmlWriter for uri, with no compression. */
  if ((writer = xmlNewTextWriterFilename(XHFname, 0)) == NULL) {
    fprintf(stderr,"\nError creating the xml writer\n");
    errno = EACCES;
    return ;
  }

  /* Start the document with the xml default for the version,
   * encoding UTF-8 and the default for the standalone declaration. */
  if ( (rc = xmlTextWriterStartDocument(writer, NULL, MY_ENCODING, NULL)) < 0 ){
    fprintf(stderr,"\nError at xmlTextWriterStartDocument\n");
    return;
  }

  /***********************
   * Header Comment
   * ********************/
  tmp = ConvertInput("HATS XML Header File \nBased in the SSTDataDescription.xsd this XML describes the Subintegration Raw Binary Data.\nInitialDate and InitialTime correspond to the file creation and must be fullfilled by the acquisition system.\n\nVersion: 0.9    (Preliminary )\n\nAuthor -   Guigue - 2019-11-01\n",MY_ENCODING);
  if ( (rc = xmlTextWriterWriteComment(writer, tmp)) < 0){
    fprintf(stderr,"\nError at xmlTextWriterWriteComment\n");
    errno= EACCES;
    return;
  }
  if (tmp != NULL) xmlFree(tmp);

  /****************************************
   * First Elements SSTDataFormat
   ****************************************/ 
  if ( (rc = xmlTextWriterStartElement(writer, BAD_CAST "SSTDataFormat")) < 0 ){
    fprintf(stderr,"\nError at xmlTextWriterStartElement\n");
    errno = EACCES;
    return;
  }
  /* Atributes of SSTDataFormat */
  if (  (rc = xmlTextWriterWriteAttribute(writer, BAD_CAST "DataType",
					  BAD_CAST "Auxiliary Position") ) < 0) {
    fprintf(stderr,"\nError at xmlTextWriterWriteAttribute\n");
    errno = EACCES;
    return;
  }
  if (  (rc = xmlTextWriterWriteAttribute(writer, BAD_CAST "InitialDate",
					  BAD_CAST initial_date) ) < 0) {
    fprintf(stderr,"\nError at xmlTextWriterWriteAttribute\n");
    errno = EACCES;
    return;
  }
  if (  (rc = xmlTextWriterWriteAttribute(writer, BAD_CAST "InitialTime",
					  BAD_CAST initial_time) ) < 0) {
    fprintf(stderr,"\nError at xmlTextWriterWriteAttribute\n");
    errno = EACCES;
    return;
  }
  if (  (rc = xmlTextWriterWriteAttribute(writer, BAD_CAST "xmlns:xsi",
					  BAD_CAST "http://www.w3.org/2001/XMLSchema-instance") ) < 0) {
    fprintf(stderr,"\nError at xmlTextWriterWriteAttribute\n");
    errno = EACCES;
    return;
  }

  if (  (rc = xmlTextWriterWriteAttribute(writer, BAD_CAST "xsi:noNamespaceSchemaLocation",
					  BAD_CAST "SSTDataDescription.xsd") ) < 0) {
    fprintf(stderr,"\nError at xmlTextWriterWriteAttribute\n");
    errno = EACCES;
    return;
  }
  /*  ********* END Header Elements ****************************/


  
  /* Include the different Elements. All are of type SSTDataFormat, but they
     have different attributes and elements.
  */

  /********************** time ************************************/
  if ( (rc = xmlTextWriterStartElement(writer, BAD_CAST "SSTDataVariable")) < 0){
    fprintf(stderr,"testXmlwriterFilename: Error at xmlTextWriterStartElement\n");
    errno = EACCES;
    return;
  }
  /* Attribute */
  if (  (rc = xmlTextWriterWriteAttribute(writer, BAD_CAST "VarName",
					  BAD_CAST ConvertInput("time", MY_ENCODING)) ) < 0) {
    fprintf(stderr,"\nError at xmlTextWriterWriteAttribute\n");
    errno = EACCES;
    return;
  }

  /* Elements */
  if (  (rc = xmlTextWriterWriteElement(writer, BAD_CAST "VarName", ConvertInput("time", MY_ENCODING)) ) < 0){
    fprintf(stderr,"testXmlwriterFilename: Error at xmlTextWriterWriteFormatElement\n");
    errno = EACCES;
    return;
  }
  if (  (rc = xmlTextWriterWriteFormatElement(writer, BAD_CAST "VarLength","%1.1d", 1) ) < 0){
    fprintf(stderr,"testXmlwriterFilename: Error at xmlTextWriterWriteFormatElement\n");
    errno = EACCES;
    return;
  }
  if (  (rc = xmlTextWriterWriteElement(writer, BAD_CAST "VarType", ConvertInput("xs:unsignedLong", MY_ENCODING))) < 0){
    fprintf(stderr,"testXmlwriterFilename: Error at xmlTextWriterWriteFormatElement\n");
    errno = EACCES;
    return;
  }
  if (  (rc = xmlTextWriterWriteElement(writer, BAD_CAST "VarUnit", ConvertInput("Husecond", MY_ENCODING)) ) < 0){
    fprintf(stderr,"testXmlwriterFilename: Error at xmlTextWriterWriteFormatElement\n");
    errno = EACCES;
    return;
  }
  /* Close the element. */
  if (  (rc = xmlTextWriterEndElement(writer)) < 0){
    fprintf(stderr,"testXmlwriterFilename: Error at xmlTextWriterEndElement\n");
    errno = EACCES;
    return;
  }

  /********************** Julian Day ******************************************************************/
  if ( (rc = xmlTextWriterStartElement(writer, BAD_CAST "SSTDataVariable")) < 0){
    fprintf(stderr,"testXmlwriterFilename: Error at xmlTextWriterStartElement\n");
    errno = EACCES;
    return;
  }
  /* Attribute */
  if (  (rc = xmlTextWriterWriteAttribute(writer, BAD_CAST "VarName",
					  BAD_CAST ConvertInput("JD", MY_ENCODING)) ) < 0) {
    fprintf(stderr,"\nError at xmlTextWriterWriteAttribute\n");
    errno = EACCES;
    return;
  }

  /* Elements */
  if (  (rc = xmlTextWriterWriteElement(writer, BAD_CAST "VarName", ConvertInput("time", MY_ENCODING)) ) < 0){
    fprintf(stderr,"testXmlwriterFilename: Error at xmlTextWriterWriteFormatElement\n");
    errno = EACCES;
    return;
  }
  if (  (rc = xmlTextWriterWriteFormatElement(writer, BAD_CAST "VarLength","%1.1d", 1) ) < 0){
    fprintf(stderr,"testXmlwriterFilename: Error at xmlTextWriterWriteFormatElement\n");
    errno = EACCES;
    return;
  }
  if (  (rc = xmlTextWriterWriteElement(writer, BAD_CAST "VarType", ConvertInput("xs:double", MY_ENCODING))) < 0){
    fprintf(stderr,"testXmlwriterFilename: Error at xmlTextWriterWriteFormatElement\n");
    errno = EACCES;
    return;
  }
  if (  (rc = xmlTextWriterWriteElement(writer, BAD_CAST "VarUnit", ConvertInput("JD", MY_ENCODING)) ) < 0){
    fprintf(stderr,"testXmlwriterFilename: Error at xmlTextWriterWriteFormatElement\n");
    errno = EACCES;
    return;
  }
  /* Close the element. */
  if (  (rc = xmlTextWriterEndElement(writer)) < 0){
    fprintf(stderr,"testXmlwriterFilename: Error at xmlTextWriterEndElement\n");
    errno = EACCES;
    return;
  }

  /********************** Sidereal Time  *************************************************/
  if ( (rc = xmlTextWriterStartElement(writer, BAD_CAST "SSTDataVariable")) < 0){
    fprintf(stderr,"testXmlwriterFilename: Error at xmlTextWriterStartElement\n");
    errno = EACCES;
    return;
  }
  /* Attribute */
  if (  (rc = xmlTextWriterWriteAttribute(writer, BAD_CAST "VarName",
					  BAD_CAST ConvertInput("sidTime", MY_ENCODING)) ) < 0) {
    fprintf(stderr,"\nError at xmlTextWriterWriteAttribute\n");
    errno = EACCES;
    return;
  }

  /* Elements */
  if (  (rc = xmlTextWriterWriteElement(writer, BAD_CAST "VarName", ConvertInput("sidTime", MY_ENCODING)) ) < 0){
    fprintf(stderr,"testXmlwriterFilename: Error at xmlTextWriterWriteFormatElement\n");
    errno = EACCES;
    return;
  }
  if (  (rc = xmlTextWriterWriteFormatElement(writer, BAD_CAST "VarLength","%1.1d", 1) ) < 0){
    fprintf(stderr,"testXmlwriterFilename: Error at xmlTextWriterWriteFormatElement\n");
    errno = EACCES;
    return;
  }
  if (  (rc = xmlTextWriterWriteElement(writer, BAD_CAST "VarType", ConvertInput("xs:double", MY_ENCODING))) < 0){
    fprintf(stderr,"testXmlwriterFilename: Error at xmlTextWriterWriteFormatElement\n");
    errno = EACCES;
    return;
  }
  if (  (rc = xmlTextWriterWriteElement(writer, BAD_CAST "VarUnit", ConvertInput("hour", MY_ENCODING)) ) < 0){
    fprintf(stderr,"testXmlwriterFilename: Error at xmlTextWriterWriteFormatElement\n");
    errno = EACCES;
    return;
  }
  /* Close the element. */
  if (  (rc = xmlTextWriterEndElement(writer)) < 0){
    fprintf(stderr,"testXmlwriterFilename: Error at xmlTextWriterEndElement\n");
    errno = EACCES;
    return;
  }

  /********************** Altitude (Elevation)  *************************************************/
  if ( (rc = xmlTextWriterStartElement(writer, BAD_CAST "SSTDataVariable")) < 0){
    fprintf(stderr,"testXmlwriterFilename: Error at xmlTextWriterStartElement\n");
    errno = EACCES;
    return;
  }
  /* Attribute */
  if (  (rc = xmlTextWriterWriteAttribute(writer, BAD_CAST "VarName",
					  BAD_CAST ConvertInput("altitude", MY_ENCODING)) ) < 0) {
    fprintf(stderr,"\nError at xmlTextWriterWriteAttribute\n");
    errno = EACCES;
    return;
  }

  /* Elements */
  if (  (rc = xmlTextWriterWriteElement(writer, BAD_CAST "VarName", ConvertInput("altitude", MY_ENCODING)) ) < 0){
    fprintf(stderr,"testXmlwriterFilename: Error at xmlTextWriterWriteFormatElement\n");
    errno = EACCES;
    return;
  }
  if (  (rc = xmlTextWriterWriteFormatElement(writer, BAD_CAST "VarLength","%1.1d", 1) ) < 0){
    fprintf(stderr,"testXmlwriterFilename: Error at xmlTextWriterWriteFormatElement\n");
    errno = EACCES;
    return;
  }
  if (  (rc = xmlTextWriterWriteElement(writer, BAD_CAST "VarType", ConvertInput("xs:double", MY_ENCODING))) < 0){
    fprintf(stderr,"testXmlwriterFilename: Error at xmlTextWriterWriteFormatElement\n");
    errno = EACCES;
    return;
  }
  if (  (rc = xmlTextWriterWriteElement(writer, BAD_CAST "VarUnit", ConvertInput("Degrees", MY_ENCODING)) ) < 0){
    fprintf(stderr,"testXmlwriterFilename: Error at xmlTextWriterWriteFormatElement\n");
    errno = EACCES;
    return;
  }
  /* Close the element. */
  if (  (rc = xmlTextWriterEndElement(writer)) < 0){
    fprintf(stderr,"testXmlwriterFilename: Error at xmlTextWriterEndElement\n");
    errno = EACCES;
    return;
  }

  /********************** Azimuth ****************************************/
  if ( (rc = xmlTextWriterStartElement(writer, BAD_CAST "SSTDataVariable")) < 0){
    fprintf(stderr,"testXmlwriterFilename: Error at xmlTextWriterStartElement\n");
    errno = EACCES;
    return;
  }
  /* Attribute */
  if (  (rc = xmlTextWriterWriteAttribute(writer, BAD_CAST "VarName",
					  BAD_CAST ConvertInput("azimuth", MY_ENCODING)) ) < 0) {
    fprintf(stderr,"\nError at xmlTextWriterWriteAttribute\n");
    errno = EACCES;
    return;
  }

  /* Elements */
  if (  (rc = xmlTextWriterWriteElement(writer, BAD_CAST "VarName", ConvertInput("azimuth", MY_ENCODING)) ) < 0){
    fprintf(stderr,"testXmlwriterFilename: Error at xmlTextWriterWriteFormatElement\n");
    errno = EACCES;
    return;
  }
  if (  (rc = xmlTextWriterWriteFormatElement(writer, BAD_CAST "VarLength","%1.1d", 1) ) < 0){
    fprintf(stderr,"testXmlwriterFilename: Error at xmlTextWriterWriteFormatElement\n");
    errno = EACCES;
    return;
  }
  if (  (rc = xmlTextWriterWriteElement(writer, BAD_CAST "VarType", ConvertInput("xs:double", MY_ENCODING))) < 0){
    fprintf(stderr,"testXmlwriterFilename: Error at xmlTextWriterWriteFormatElement\n");
    errno = EACCES;
    return;
  }
  if (  (rc = xmlTextWriterWriteElement(writer, BAD_CAST "VarUnit", ConvertInput("Degrees", MY_ENCODING)) ) < 0){
    fprintf(stderr,"testXmlwriterFilename: Error at xmlTextWriterWriteFormatElement\n");
    errno = EACCES;
    return;
  }
  /* Close the element. */
  if (  (rc = xmlTextWriterEndElement(writer)) < 0){
    fprintf(stderr,"testXmlwriterFilename: Error at xmlTextWriterEndElement\n");
    errno = EACCES;
    return;
  }

  
  /********************** Right Ascension ****************************************/
  if ( (rc = xmlTextWriterStartElement(writer, BAD_CAST "SSTDataVariable")) < 0){
    fprintf(stderr,"testXmlwriterFilename: Error at xmlTextWriterStartElement\n");
    errno = EACCES;
    return;
  }
  /* Attribute */
  if (  (rc = xmlTextWriterWriteAttribute(writer, BAD_CAST "VarName",
					  BAD_CAST ConvertInput("RA", MY_ENCODING)) ) < 0) {
    fprintf(stderr,"\nError at xmlTextWriterWriteAttribute\n");
    errno = EACCES;
    return;
  }

  /* Elements */
  if (  (rc = xmlTextWriterWriteElement(writer, BAD_CAST "VarName", ConvertInput("RA", MY_ENCODING)) ) < 0){
    fprintf(stderr,"testXmlwriterFilename: Error at xmlTextWriterWriteFormatElement\n");
    errno = EACCES;
    return;
  }
  if (  (rc = xmlTextWriterWriteFormatElement(writer, BAD_CAST "VarLength","%1.1d", 1) ) < 0){
    fprintf(stderr,"testXmlwriterFilename: Error at xmlTextWriterWriteFormatElement\n");
    errno = EACCES;
    return;
  }
  if (  (rc = xmlTextWriterWriteElement(writer, BAD_CAST "VarType", ConvertInput("xs:double", MY_ENCODING))) < 0){
    fprintf(stderr,"testXmlwriterFilename: Error at xmlTextWriterWriteFormatElement\n");
    errno = EACCES;
    return;
  }
  if (  (rc = xmlTextWriterWriteElement(writer, BAD_CAST "VarUnit", ConvertInput("Hour", MY_ENCODING)) ) < 0){
    fprintf(stderr,"testXmlwriterFilename: Error at xmlTextWriterWriteFormatElement\n");
    errno = EACCES;
    return;
  }
  /* Close the element. */
  if (  (rc = xmlTextWriterEndElement(writer)) < 0){
    fprintf(stderr,"testXmlwriterFilename: Error at xmlTextWriterEndElement\n");
    errno = EACCES;
    return;
  }

  /********************** Declination ****************************************/
  if ( (rc = xmlTextWriterStartElement(writer, BAD_CAST "SSTDataVariable")) < 0){
    fprintf(stderr,"testXmlwriterFilename: Error at xmlTextWriterStartElement\n");
    errno = EACCES;
    return;
  }
  /* Attribute */
  if (  (rc = xmlTextWriterWriteAttribute(writer, BAD_CAST "VarName",
					  BAD_CAST ConvertInput("Dec", MY_ENCODING)) ) < 0) {
    fprintf(stderr,"\nError at xmlTextWriterWriteAttribute\n");
    errno = EACCES;
    return;
  }

  /* Elements */
  if (  (rc = xmlTextWriterWriteElement(writer, BAD_CAST "VarName", ConvertInput("Dec", MY_ENCODING)) ) < 0){
    fprintf(stderr,"testXmlwriterFilename: Error at xmlTextWriterWriteFormatElement\n");
    errno = EACCES;
    return;
  }
  if (  (rc = xmlTextWriterWriteFormatElement(writer, BAD_CAST "VarLength","%1.1d", 1) ) < 0){
    fprintf(stderr,"testXmlwriterFilename: Error at xmlTextWriterWriteFormatElement\n");
    errno = EACCES;
    return;
  }
  if (  (rc = xmlTextWriterWriteElement(writer, BAD_CAST "VarType", ConvertInput("xs:double", MY_ENCODING))) < 0){
    fprintf(stderr,"testXmlwriterFilename: Error at xmlTextWriterWriteFormatElement\n");
    errno = EACCES;
    return;
  }
  if (  (rc = xmlTextWriterWriteElement(writer, BAD_CAST "VarUnit", ConvertInput("Degrees", MY_ENCODING)) ) < 0){
    fprintf(stderr,"testXmlwriterFilename: Error at xmlTextWriterWriteFormatElement\n");
    errno = EACCES;
    return;
  }
  /* Close the element. */
  if (  (rc = xmlTextWriterEndElement(writer)) < 0){
    fprintf(stderr,"testXmlwriterFilename: Error at xmlTextWriterEndElement\n");
    errno = EACCES;
    return;
  }

  /********************** Tracking Rate ****************************************
                          Right Ascension                                      */
  
  if ( (rc = xmlTextWriterStartElement(writer, BAD_CAST "SSTDataVariable")) < 0){
    fprintf(stderr,"testXmlwriterFilename: Error at xmlTextWriterStartElement\n");
    errno = EACCES;
    return;
  }
  /* Attribute */
  if (  (rc = xmlTextWriterWriteAttribute(writer, BAD_CAST "VarName",
					  BAD_CAST ConvertInput("RaRate", MY_ENCODING)) ) < 0) {
    fprintf(stderr,"\nError at xmlTextWriterWriteAttribute\n");
    errno = EACCES;
    return;
  }

  /* Elements */
  if (  (rc = xmlTextWriterWriteElement(writer, BAD_CAST "VarName", ConvertInput("RaRate", MY_ENCODING)) ) < 0){
    fprintf(stderr,"testXmlwriterFilename: Error at xmlTextWriterWriteFormatElement\n");
    errno = EACCES;
    return;
  }
  if (  (rc = xmlTextWriterWriteFormatElement(writer, BAD_CAST "VarLength","%1.1d", 1) ) < 0){
    fprintf(stderr,"testXmlwriterFilename: Error at xmlTextWriterWriteFormatElement\n");
    errno = EACCES;
    return;
  }
  if (  (rc = xmlTextWriterWriteElement(writer, BAD_CAST "VarType", ConvertInput("xs:double", MY_ENCODING))) < 0){
    fprintf(stderr,"testXmlwriterFilename: Error at xmlTextWriterWriteFormatElement\n");
    errno = EACCES;
    return;
  }
  if (  (rc = xmlTextWriterWriteElement(writer, BAD_CAST "VarUnit", ConvertInput("arcsec/second", MY_ENCODING)) ) < 0){
    fprintf(stderr,"testXmlwriterFilename: Error at xmlTextWriterWriteFormatElement\n");
    errno = EACCES;
    return;
  }
  /* Close the element. */
  if (  (rc = xmlTextWriterEndElement(writer)) < 0){
    fprintf(stderr,"testXmlwriterFilename: Error at xmlTextWriterEndElement\n");
    errno = EACCES;
    return;
  }

  /*                        Declination                                         */
  
  if ( (rc = xmlTextWriterStartElement(writer, BAD_CAST "SSTDataVariable")) < 0){
    fprintf(stderr,"testXmlwriterFilename: Error at xmlTextWriterStartElement\n");
    errno = EACCES;
    return;
  }
  /* Attribute */
  if (  (rc = xmlTextWriterWriteAttribute(writer, BAD_CAST "VarName",
					  BAD_CAST ConvertInput("DecRate", MY_ENCODING)) ) < 0) {
    fprintf(stderr,"\nError at xmlTextWriterWriteAttribute\n");
    errno = EACCES;
    return;
  }

  /* Elements */
  if (  (rc = xmlTextWriterWriteElement(writer, BAD_CAST "VarName", ConvertInput("DecRate", MY_ENCODING)) ) < 0){
    fprintf(stderr,"testXmlwriterFilename: Error at xmlTextWriterWriteFormatElement\n");
    errno = EACCES;
    return;
  }
  if (  (rc = xmlTextWriterWriteFormatElement(writer, BAD_CAST "VarLength","%1.1d", 1) ) < 0){
    fprintf(stderr,"testXmlwriterFilename: Error at xmlTextWriterWriteFormatElement\n");
    errno = EACCES;
    return;
  }
  if (  (rc = xmlTextWriterWriteElement(writer, BAD_CAST "VarType", ConvertInput("xs:double", MY_ENCODING))) < 0){
    fprintf(stderr,"testXmlwriterFilename: Error at xmlTextWriterWriteFormatElement\n");
    errno = EACCES;
    return;
  }
  if (  (rc = xmlTextWriterWriteElement(writer, BAD_CAST "VarUnit", ConvertInput("arcsec/second", MY_ENCODING)) ) < 0){
    fprintf(stderr,"testXmlwriterFilename: Error at xmlTextWriterWriteFormatElement\n");
    errno = EACCES;
    return;
  }
  if (  (rc = xmlTextWriterEndElement(writer)) < 0){
    fprintf(stderr,"testXmlwriterFilename: Error at xmlTextWriterEndElement\n");
    errno = EACCES;
    return;
  }

  /*******       End of Elements in the XML Header File                             *************/
  
    
  /* Here we could close the elements ORDER and EXAMPLE using the
   * function xmlTextWriterEndElement, but since we do not want to
   * write any other elements, we simply call xmlTextWriterEndDocument,
   * which will do all the work. */
  if ( (rc = xmlTextWriterEndDocument(writer)) < 0 ){
    fprintf(stderr,"\nError at xmlTextWriterEndDocument\n");
    errno = EACCES;
    return;
  }
  
  xmlFreeTextWriter(writer);

  
  /*
   * Cleanup function for the XML library.
   */
  xmlCleanupParser();
  /*
   * this is to debug memory for regression tests
   */
  xmlMemoryDump();
  return ;
}

/**
 * ConvertInput:
 * @in: string in a given encoding
 * @encoding: the encoding used
 *
 * Converts @in into UTF-8 for processing with libxml2 APIs
 *
 * Returns the converted UTF-8 string, or NULL in case of error.
 */
xmlChar *
ConvertInput(const char *in, const char *encoding)
{
    xmlChar *out;
    int ret;
    int size;
    int out_size;
    int temp;
    xmlCharEncodingHandlerPtr handler;

    if (in == 0)
        return 0;

    handler = xmlFindCharEncodingHandler(encoding);

    if (!handler) {
        printf("ConvertInput: no encoding handler found for '%s'\n",
               encoding ? encoding : "");
        return 0;
    }

    size = (int) strlen(in) + 1;
    out_size = size * 2 - 1;
    out = (unsigned char *) xmlMalloc((size_t) out_size);

    if (out != 0) {
        temp = size - 1;
        ret = handler->input(out, &out_size, (const xmlChar *) in, &temp);
        if ((ret < 0) || (temp - size + 1)) {
            if (ret < 0) {
                printf("ConvertInput: conversion wasn't successful.\n");
            } else {
                printf
                    ("ConvertInput: conversion wasn't successful. converted: %i octets.\n",
                     temp);
            }

            xmlFree(out);
            out = 0;
        } else {
            out = (unsigned char *) xmlRealloc(out, out_size + 1);
            out[out_size] = 0;  /*null terminating out */
        }
    } else {
        printf("ConvertInput: no mem\n");
    }

    return out;
}

#endif
