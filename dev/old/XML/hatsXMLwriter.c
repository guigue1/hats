/**
 * hatsXMLWrite
 * synopsis: Test Program, for the XML writers
 * author: Guigue
 *         2019-11-11
 *********************************/

#include <stdio.h>
#include <string.h>
#include <time.h>
#include "hatsXMLwriter.h"
#include "hatsStructures.h"

extern void XMLwriter_Header_Auxiliary_Position(char * , char * , char * );
extern void XMLWriter_Create_Table_Auxiliary_Observing_Modes(HatsTargetInfo, char *);
extern void XMLWriter_Append_Table_Auxiliary_Observing_Modes(HatsTargetInfo, char *);

void main()
{
  char XHFname[160],
    xmlTableName[160],
    initial_date[40],
    initial_time[40];
  time_t now;
  struct tm * tm_now;
  HatsTargetInfo target, * ptr_target;


  /*****************************************************************
     Example 1
     Create a XML Header File for Auxiliary Position Data

  now = time(NULL);
  tm_now = gmtime(&now);
  sprintf(XHFname,"hats_header_Auxiliary_Position-%4.4d-%2.2d-%2.2dT%2.2d:%2.2d:%2.2d.xml",
	  tm_now->tm_year+1900,
	  tm_now->tm_mon+1,
	  tm_now->tm_mday,
	  tm_now->tm_hour,
	  tm_now->tm_min,
	  tm_now->tm_sec);
  
  sprintf(initial_date,"%4.4d-%2.2d-%2.2d",
	  tm_now->tm_year+1900,
	  tm_now->tm_mon+1,
	  tm_now->tm_mday);
  
  sprintf(initial_time,"%2.2d:%2.2d:%2.2d UTC",
	  tm_now->tm_hour,
	  tm_now->tm_min,
	  tm_now->tm_sec);
  
  XMLwriter_Header_Auxiliary_Position(XHFname, initial_date, initial_time);
  *******************************************************************/


  /******************************************************************
   Example 2
   Create a XML Table Data with Observing Modes
  *******************************************************************/

  now = time(NULL);
  tm_now = gmtime(&now);
  sprintf(xmlTableName,"hats_table_Auxiliary_Observing_Mode-%4.4d-%2.2d-%2.2d.xml",
	  tm_now->tm_year+1900,
	  tm_now->tm_mon+1,
	  tm_now->tm_mday);

  ptr_target = &target;
  sprintf(ptr_target->initial_date,"%s","2011-11-12T15:20:12");
  sprintf(ptr_target->final_date,"%s","0000-00-00T00:00:00");
  ptr_target->initial_time=554273450;
  ptr_target->final_time=0;
  sprintf(ptr_target->tname,"%s","Sun");
  sprintf(ptr_target->cname,"%s","Sun");
  sprintf(ptr_target->rise_time,"%s","11:23:45");
  sprintf(ptr_target->set_time,"%s","18:45:56");
  sprintf(ptr_target->transit_time,"%s","15:12:34");
  sprintf(ptr_target->observing_mode,"%s","tracking_center");
  ptr_target->initial_jd=2458799.13902776;
  ptr_target->final_jd=0;
  ptr_target->azimuth_rise=115.123456;
  ptr_target->azimuth_set=267.876543;
  ptr_target->sun_helio_long=14.520246;
  ptr_target->sun_helio_lat=3.2954898;
  ptr_target->sun_pos_angle=22.356791;
  ptr_target->sun_dist_ly=8.235110;

  //  XMLWriter_Create_Table_Auxiliary_Observing_Modes(target,xmlTableName);


  /******************************************************************
   Example 3
   Add and entry in the XML Table Data with Observing Modes

  
  now = time(NULL);
  tm_now = gmtime(&now);
  sprintf(xmlTableName,"hats_table_Auxiliary_Observing_Mode-%4.4d-%2.2d-%2.2d.xml",
	  tm_now->tm_year+1900,
	  tm_now->tm_mon+1,
	  tm_now->tm_mday);
  *******************************************************************/
  
   XMLWriter_Append_Table_Auxiliary_Observing_Modes(target, xmlTableName);

  
  return;

}

