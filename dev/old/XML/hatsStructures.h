#ifndef HATSSTRUCTURES
#define HATSSTRUCTURES

typedef struct {
  unsigned long long initial_time,
    final_time;
  char initial_date[80],
    final_date[80],
    tname[80],
    rise_time[40],
    set_time[40],
    transit_time[40],
    cname[80],
    observing_mode[80];
  double initial_jd,
    final_jd,
    azimuth_rise,
    azimuth_set,
    sun_helio_long,
    sun_helio_lat,
    sun_pos_angle,
    sun_dist_ly;
} HatsTargetInfo;


#endif
