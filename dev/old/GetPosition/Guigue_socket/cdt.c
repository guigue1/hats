/*
 * 
 * 
 * CDT: Client Data Transfer
 *      Transfer values from the shared memory struct_send_data1
 *      and send them through TCP/IP to Display Computer (robinson)
 *
 * Note: Must be started on acqusition node (node2) !
 *
 *
 *
 *
 * Guigue - May 1998 - Bern
 */

#include <process.h>
#include <stdio.h>                
#include <stdlib.h>               
#include <string.h>
#include <signal.h>
#include <setjmp.h>
#include <syslog.h>
#include <sys/psinfo.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/name.h>
#include <sys/proxy.h>
#include <unistd.h>               
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <i86.h>
#include <signal.h>
#include <time.h>
#include <sys/kernel.h>

#include "data_transfer.h"
#include "atmio16x.h"
#include "files.h"
#include "buffers.h"
//#include "struct1.h"
#include "struct2.h"

extern void   log_msg( unsigned short int, char *, unsigned short int);

void open_shmem2() ;
void open_connection ( void ) ;
void get_socket ( void )      ;
void BrokenPipeCatch( int )   ;
void ExitOnDemand ( int )     ;
void ExitCleanly ( void )     ;


char *  prname = "cdt: "; // name for the log_msg routine

/* Structures and variables for the Socket Connection */
int clientSocket      ,
    remotePort = PORT ,
    status     = 0    ;
struct hostent * hostPtr      = NULL       ;
struct sockaddr_in serverName = { 0 }      ;
char *remoteHost              = TCP_SERVER ;

char *buffer                               ; // pointer to the data
int size_of_data                           ; // size of the data structure

/* Structures for signal handling */
sigjmp_buf env                             ; 
sigset_t set                               ;

/* Proxy and Timer declarations   */
pid_t 	proxy             ;                  
timer_t  id              ;
struct itimerspec timer  ;
struct sigevent event    ;

main()
{


  /* Register a function to be called when the program terminates normally

     This function is called whenever you do a "kill..." externally and
     when the program finish.  As "CDT never dies", it's only for the
     external kill operation

     */

    atexit( ExitCleanly ) ;

    /* First register the program behind syslogd() */

    putenv("SYSLOG=1")                       ;
    openlog("cdt", LOG_NDELAY, LOG_OTHER)    ;
    log_msg(LOG_TCPIP,">>> start sending",NOTICE) ;

    open_shmem2( )                           ;
    buffer = (char *) send_data1_ptr         ;
    size_of_data = sizeof(struct_send_data1) ;

   /* attach the global name "/sst/cdt" to the current process */
    qnx_name_attach(0 , "/sst/cdt" );


    /*=-=-=-=- Timer Configuration=-=-=-=-=-=-  */

    /* set timer to intervall of 1 minute
    and activate proxy to trigger observing
    cycle when 1 minute is elapsed:             */

    /* get a proxy for the timer to kick */
    proxy = qnx_proxy_attach(0, 0, 0, -1) ;
    if (proxy == -1) {
      printf("\n\nunable to attach proxy\n\n");    	
      printf("<<< killed\n\n\n");    
      exit(1) ;
    }

    /* attach to the timer */
    event.sigev_signo = -proxy ;
    id = timer_create( CLOCK_REALTIME, &event) ;
    if (id == -1) {
    log_msg(LOG_OTHER,"unable to attach timer",NOTICE);    	
    log_msg(LOG_CTRL,"<<< killed",ALERT);    	
    exit(1) ;
    }

    /* set timer to 0 seconds for initial wait
    and to int_intv milliseconds periodicity   */
    timer.it_value.tv_sec     = 1L ;	
    timer.it_value.tv_nsec    = 0L ;	
    timer.it_interval.tv_sec  = 0L ;	
    timer.it_interval.tv_nsec = WAITING_DELAY*1000000L ;
    timer_settime( id, 0, &timer, NULL) ;

    /*=-=-=-=- End of Timer Configuration=-=-=-=-=*/

    /* fill_data( )       ;                    put some numbers to try.  
					       REMOVE in final release */
    /* Get the Internet Adress Format Socket, see the function
       for more comments 
       */
    get_socket( )      ;

    /* Establish the connection, see the function for more comments */
    open_connection()  ;

    /* Catch some signals */
    signal( SIGPIPE, BrokenPipeCatch) ;     // Catch a Broken Pipe signal
    qnx_pflags( ~0,_PPF_IMMORTAL ,0,0) ;    // allows KILL to be caught
    signal( SIGKILL, ExitOnDemand )   ;     // Catch a KILL signal
    signal( SIGINT , ExitOnDemand )   ;     // Catch a ^C

    while (1){                              // CDT never dies...
       int ret_val ;

       /* 

	  RETURN POINT

	  Save the current environment process and the signal mask.
	  In that way, we can return to this point from BrokenPipeCatch
       */
       ret_val = sigsetjmp( env , 1 ) ;

       /* Wait until the buffer is filled, and reset the flag */
       while (!fbuf_ptr->send1_flg) Receive( proxy, 0, 0);


       /* Write to the Internet Socket.
          If the number of written bytes (status) is lower than 
          buffer size, alert the operator.  In any case, wait some time
	  until continue writing (I'm not sure whether is necessary)
       */  
       if (size_of_data > (status = write(clientSocket,buffer,size_of_data)))
        log_msg(LOG_TCPIP, "Incomplete transmission with server ", ALERT);
       delay(TRANSFER_DELAY)   ;
       fbuf_ptr->send1_flg = 0 ;
    }

}

/*************************** END OF MAIN ********************************/

/* ExitOnDemand

   Catch the SIGKILL and SIGINT (^C)

*/

void ExitOnDemand ( int sig_no ) {
  close(clientSocket)   ;
  log_msg(LOG_TCPIP, "Aborted by user", ALERT);
  closelog()            ;
  _exit(0)              ;
}

/* ExitCleanly

   For use from an external "killer" as "kill ...."

*/

void ExitCleanly ( ) {
  close(clientSocket)   ;
  log_msg(LOG_TCPIP, "Aborted ", ALERT);
  closelog()            ;
  _exit(0)              ;
}

/*
 BrokenPipeCatch

   Catch a Broken Pipe Signal (connection lost)
   and try to restablish the connection with display server

*/
 
void BrokenPipeCatch ( int sig_no ) {
	set=0x1000     ;
        /*

	  mask SIGPIPE
	  This is needed to avoid conflict between two succesiv calls

	*/
	sigprocmask( SIG_BLOCK, &set, NULL );  

	/* Alert the operator */
        log_msg(LOG_TCPIP, "Broken Pipe ", ALERT);

        /* Close the socket */
	close(clientSocket)   ;

        /* Get a new one */
	get_socket ()         ;

        /* Re-establish the connection */
	open_connection ( )   ;

        /* Return to the RETURN POINT, defined by  sigsetjmp */
	siglongjmp( env, 1 )  ;
}  


/* open_connection

   Connect if possible
   if not, retry the connection

*/

void open_connection ( void ) {

  /* Try to connect with server, which is defined in serverName:

     serverName.hostPtr        IP adress
     serverName.sin_port       Server Port

     The socket, in the client side, is defined in get_socket.

     If the connection cannot be established, retry, but before, write 
     a message to operator and log book, close the socket, and get a new one.

     I'm not sure if it is necessary to close and get new socket.
     My experience is, that if you close/open a file descriptor (a socket
     is a file descriptor, isn't it?) many many times and very fast, this
     stucks the program. Probably you run out the range number.  As I didn't
     have problems YET, I leave it as it is now. (Comment from October/98)

     In tests run at IAP in November/98 I found that effectively if the 
     connection is broken a new client socket is needed.  I decided to
     increase the delay time using a new constant called SOCKET_DELAY,
     which I set to 1 second. (23/11/98)

     In principle,as both machines are close one to the other, the 
     connection should be opened inmediately.  But,...

   */

    while ( -1 == (status = connect(clientSocket, 
				    (struct sockaddr*) &serverName,
				    sizeof(serverName))))
      {
           log_msg( LOG_TCPIP,"cannot open connection with server",ALERT);
	   close(clientSocket)   ;
           delay(SOCKET_DELAY)   ;
	   get_socket ()         ; 
      }

    log_msg( LOG_TCPIP,"connection established with Server",ALERT);

return;

}

/* get_socket

   Get an Internet Socket to connect with display server
   Get the IP number also

   */
 
void get_socket ( void ){


  /* Get a Socket for communication
     Socket will be of type AF_INET (Internet Domain Adress Format)
     Stream, SOCK_STREAM
     TCP/IP protocol, IPPROTO_TCP 

     In general, the while loop should just run once, it is suposed
     the Computer will not have a lot of IPC sockets in use :-)

  */
     
  while ( -1 == (clientSocket = 
		 socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)))
         log_msg( LOG_TCPIP,"cannot get a socket to communicate",NOTICE);

     /*
        Need to resolve the remote server IP address

	In fact this should be changed.  We *know* the IP address,
	but, I couldn't succeed to define "by hand" the hostPtr structure.
	gethostbyname() does the job for me.  
	
	Sometimes the client cannot resolve server address.  To avoid the
	interrupt of the connection, I put a cycle. (Nov/98 - IAP)

     */

     if ( NULL == (hostPtr = gethostbyname(remoteHost)))
     {

            log_msg( LOG_TCPIP,"error resolving server address ",NOTICE)  ;

     } else {

       serverName.sin_family = AF_INET            ;

       /* Convert 16- and 32-bit quantities from host-byte order to 
	  network-byte order. Needed because we transfer data over the net.
	  This is the port on the Server Side, which was arbitrarily but
	  easy-to-remember chosen to be: 12345

	  Finnally, we fill in the structure serverName, the server IP address
	  and the IP address

       */
       serverName.sin_port   = htons(remotePort)  ;


       (void) memcpy(&serverName.sin_addr, hostPtr->h_addr,hostPtr->h_length);

     }

    return ;

}


