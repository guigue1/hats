/****************************************************************************
 *                                                                          *
 *   header file for the TCP/IP connection to do the data transfer between  *
 *   acquisition machine (node 2) and the on-line display machine (twain)   *
 *                                                                          *
 ****************************************************************************/

#define PORT 12345                       // Any better suggestion?         
#ifdef SST
  #define CLIENT_ALLOWED "192.168.1.10"    // Robinson
  #define TCP_SERVER  "twain"
  #define IP_SERVER   "200.144.84.55"
  #define SDTDPID     "/data/twain/display/sdtd.pid"
  #define SDTDBIN "/data/twain/bin/sdtd"
#endif

#ifdef ITA
  #define CLIENT_ALLOWED "192.9.200.2"    // beta
  #define TCP_SERVER "epsilon"
  #define IP_SERVER  "192.9.200.5"
  #define SDTDPID     "/data/epsilon/display/sdtd.pid"
  #define SDTDBIN "/data/epsilon/bin/sdtd"
#endif

#define IP_LEN 13
#define TRANSFER_DELAY 120               /* delay between succesive sendings  
                    			     a little bit less than 100 ms */
#define WAITING_DELAY  5L                /* delay for waiting the flag ON  */ 
#define SOCKET_DELAY 5000                /* delay in case the connection
					    is broken.  The client will close
 					    the socket wait SOCKET_DELAY 
					    milliseconds, and open a new one */
