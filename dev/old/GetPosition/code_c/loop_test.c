/*#include <stdio.h>
#include <time.h>

int main(void) {
	int status;
	long int d1, d2;

	for (status = 1; status <= 100; status ++) {
		printf("\rProcessando - %u%%", status);
		fflush(stdout);
		//Operações do programa
		for (d1 = 1; d2 <= 6500000; d1 ++) for(d2 = 1; d2 <= 6500000; d2 ++);
		delay(100);
}
	printf("\nFinalizado!\n");
}

*/

/*#include <stdio.h>
#include <stdlib.h>

int main(){
   int i;
   system("clear"); // Limpar a tela
   printf("Loading"); // Exibir a Palavra Loading na tela em branco

   for(i=0;i < 10; i++){ // abrir um laço de repetição com for
   system("sleep 01"); // pausa de 1 segundo
   printf("."); // escrever 1 "." na tela
   fflush(stdout); // atualizar a tela
   }

printf("Done!\n");
}
*/


/*

	Loop infinito enquanto uma tecla não é pressionada.
	==================================================

	Implementação do kbhit() na referencia baixo:
	https://www.raspberrypi.org/forums/viewtopic.php?t=188067
*/

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <termio.h>
#include <unistd.h>

bool kbhit(void)
{
    struct termios original;
    tcgetattr(STDIN_FILENO, &original);
    struct termios term;
    memcpy(&term, &original, sizeof(term));
    term.c_lflag &= ~ICANON;
    tcsetattr(STDIN_FILENO, TCSANOW, &term);
    int characters_buffered = 0;
    ioctl(STDIN_FILENO, FIONREAD, &characters_buffered);
    tcsetattr(STDIN_FILENO, TCSANOW, &original);
    bool pressed = (characters_buffered != 0);
    return pressed;
}

void main()
{
	int i = 0;
	system("clear");

	while(!kbhit())
	{
		printf("\rLoop #%d", i);
		system("sleep 2");
		fflush(stdout);
		i++;
	}
	printf("\n");
}
