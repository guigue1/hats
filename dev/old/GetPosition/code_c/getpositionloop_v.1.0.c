/*
	==============================================================
	Universidade Presbiteriana Mackenzie
	Centro de Rádio Astronomia e Astrofísica Mackenzie - CRAAM
	==============================================================

	GetPositionLoop_v1.0
	---------------------------------------------------
	Versão contendo todos os parametros selecionados para serem extraidos da
	montagem Paramount. Nesta versão os dados são apenas mostrados na tela.
	Utilizadas classes:
	sky6RASCOMTele e sky6ObjectInformation
	---------------------------------------------------

	Autor: Tiago Giorgetti
	Email: tiago.giorgetti@craam.mackenzie.br

	Histórico:
	_______________________________________________________________________________
	 Versão	|  Data		|	Atualização
	-------------------------------------------------------------------------------
	  0.1	|  06-10-2019	| Primeira versão.
	-------------------------------------------------------------------------------
	  1.0   |  31-10-2019   | Inclusão de informações do objeto e itens de tela
		|               | contadores de vetores e definicao da estrutura
	________|_______________|______________________________________________________

	Implementação do kbhit() na referencia baixo:
	https://www.raspberrypi.org/forums/viewtopic.php?t=188067 - acesso em 04-10-2019.

*/

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <termio.h>
#include <unistd.h>

#include <sys/socket.h> //socket
#include <arpa/inet.h>  //inet_addr
#include <fcntl.h>      //open(sock)
#include <unistd.h>     //close(sock)



//Parametros de conexao
#define IP_SERVER "10.0.92.12"
#define TCP_PORT 3040
#define RCV_BUFFER_SIZE 2000


bool kbhit(void)
{
	struct termios original;
	tcgetattr(STDIN_FILENO, &original);
	struct termios term;
	memcpy(&term, &original, sizeof(term));
	term.c_lflag &= ~ICANON;
	tcsetattr(STDIN_FILENO, TCSANOW, &term);
	int characters_buffered = 0;
	ioctl(STDIN_FILENO, FIONREAD, &characters_buffered);
	tcsetattr(STDIN_FILENO, TCSANOW, &original);
	bool pressed = (characters_buffered != 0);
	return pressed;
}

/* Move string a para b */
void copy_char ( char a[], char b[])
{
	int i = 0;
	while ( a[i] != '|' )
	{
		b[i] = a[i];
		i++;
	}
	b[i] = a[i];
}

/* Conta quantos elementos do vetor de vetores  */

int cont_vector_lines ( char* a[])
{
	int i = 0;
	while ( a[i] != "!")
	{
		i++;
	}
	return i;
}



int main(int argc , char *argv[])
{

	struct hatsdata
	{
		unsigned long long time_Husec;	//Time: Husec
		double time_JD;			//Time: Julian Date
		double time_Sid;		//Time: Sideral
		//--------------------------------------------------------
		double pos_tele_alt;		//Telescope Position: Altitude
		double pos_tele_az;		//Telescope Position: Azimute
		double pos_tele_ra;		//Telescope Position: Right Ascension
		double pos_tele_dec;		//Telescope Position: Declination
		//---------------------------------------------------------
		double rate_ObjId_alt;		//Tracking rate: Altitude
		double rate_ObjId_az;		//Tracking rate: Azimute
		double rate_ObjId_ra;		//Tracking rate: Right Ascension
		double rate_ObjId_dec;		//Tracking rate: Declination
	};

	long int j = 0;
	int k = 0;
	const char spin[4]={'|', '/', '-', '\\'};
	int i, sock;
	struct sockaddr_in server;
	char server_reply[RCV_BUFFER_SIZE];
	char altaz_reply[RCV_BUFFER_SIZE];
	char radec_reply[RCV_BUFFER_SIZE];
	char trackingrate_reply[RCV_BUFFER_SIZE]; // Only for RA_DEC Coordinates
	char object_name_reply[RCV_BUFFER_SIZE];
        char object_sidereal_reply[RCV_BUFFER_SIZE];
        char object_jd_reply[RCV_BUFFER_SIZE];

	//----------Begin--Of--Javascripts--------------------------------------------

	char* get_altaz[] =
	{
		"/* Java Script */",
		"/* Socket Start Packet */",
		"var Out;",
		"sky6RASCOMTele.GetAzAlt();",
		"var az = sky6RASCOMTele.dAz;",
		"var alt = sky6RASCOMTele.dAlt;",
		"Out = 'Az: ' + az.toFixed(4) + 'º ' + 'Alt= ' + alt.toFixed(4) + 'º '", //Alt+186=º
		"/* Socket End Packet */",
		"!"	//End Caracter
	};

	char* get_radec[] =
        {
                "/* Java Script */",
                "/* Socket Start Packet */",
                "var Out;",
                "sky6RASCOMTele.GetRaDec();",
                "var ra = sky6RASCOMTele.dRa;",
                "var dec = sky6RASCOMTele.dDec;",
                "Out = 'RA: ' + ra.toFixed(4) + 'º ' + 'DEC= ' + dec.toFixed(4) + 'º '", //Alt+186=º
                "/* Socket End Packet */",
		"!"	//End Caracter
        };

        char* get_radec_TrackingRate[] =
        {
                "/* Java Script */",
                "/* Socket Start Packet */",
                "var Out;",
                "sky6RASCOMTele.GetRaDec();",
                "var tra = sky6RASCOMTele.dRaTrackingRate;",
                "var tdec = sky6RASCOMTele.dDecTrackingRate;",
                "Out = 'RA rate: ' + tra.toFixed(4) + ' arcsec/sec  ' + 'DEC rate: ' + tdec.toFixed(4) + ' arcsec/sec '",
                "/* Socket End Packet */",
		"!"	//End Caracter
        };

        char* get_object_name[] =
        {
                "/* Java Script */",
                "/* Socket Start Packet */",
                "var Out;",
		"sky6ObjectInformation.Property(0);",
		"var objeto = sky6ObjectInformation.ObjInfoPropOut;",
                "Out = 'Objeto: ' + objeto ",
                "/* Socket End Packet */",
		"!"	//End Caracter
        };


        char* get_object_sidereal[] =
        {
                "/* Java Script */",
                "/* Socket Start Packet */",
                "var Out;",
                "sky6ObjectInformation.Property(173);",
                "var sidereal = sky6ObjectInformation.ObjInfoPropOut;",
                "Out = 'Sidereal Time: ' + sidereal.toFixed(4) ",
                "/* Socket End Packet */",
                "!"     //End Caracter
        };

        char* get_object_jd[] =
        {
                "/* Java Script */",
                "/* Socket Start Packet */",
                "var Out;",
                "sky6ObjectInformation.Property(174);",
                "var jd = sky6ObjectInformation.ObjInfoPropOut;",
                "Out = 'Julian Date: ' + jd.toFixed(4) ",
                "/* Socket End Packet */",
                "!"     //End Caracter
        };



	//----------End--Of--Javascripts--------------------------------------------

	system("clear");

	while(!kbhit())
	{

		//Create socket
		sock = socket(AF_INET , SOCK_STREAM , 0);
		if (sock == -1)
		{
			printf("Could not create socket");
		}
		//printf("Socket created\n");

		server.sin_addr.s_addr = inet_addr(IP_SERVER);
        	server.sin_family = AF_INET;
		server.sin_port = htons(TCP_PORT);

		//printf("ate qui!\n");

		//Connect to remote server
		if (connect(sock , (struct sockaddr *)&server , sizeof(server)) < 0)
		{
			printf("Connect failed. Error\n");
			return 1;
		}
		//printf("Connected\n");


		//------------------------------------------------------------------
		//------------ Bloco de GET para Posição Alt_Az --------------------

		// Send get data
		for(i = 0; i<cont_vector_lines(get_altaz) ; i++)
		{
			if( send(sock , get_altaz[i] , strlen(get_altaz[i]) , 0) < 0)
			{
				puts("Send failed. Error");
				return 1;
			}
		}
		//printf("Message sent\n");

		//Receive a reply from the server
		if( recv(sock , server_reply , RCV_BUFFER_SIZE , 0) < 0)
		{
			printf("Recv failed. Error\n");
		}

		//Copy char ( source , destiny )
		copy_char ( server_reply , altaz_reply );


                //------------------------------------------------------------------
                //------------ Bloco de GET para Posição RA_DEC --------------------

		//Send get data
                for(i = 0; i<cont_vector_lines(get_radec) ; i++)
                {
                        if( send(sock , get_radec[i] , strlen(get_radec[i]) , 0) < 0)
                        {
                                puts("Send failed. Error");
                                return 1;
                        }
                }
		//printf("Message sent\n");

                //Receive a reply from the server
                if( recv(sock , server_reply , RCV_BUFFER_SIZE , 0) < 0)
                {
                        printf("Recv failed. Error\n");
                }

		//Copy char ( source , destiny )
                copy_char ( server_reply , radec_reply );


                //------------------------------------------------------------------
                //--------- Bloco de GET para Tracking Rate RA_DEC -----------------

                //Send get data
                for(i = 0; i<cont_vector_lines(get_radec_TrackingRate) ; i++)
                {
                        if( send(sock , get_radec_TrackingRate[i] , strlen(get_radec_TrackingRate[i]) , 0) < 0)
                        {
                                puts("Send failed. Error");
                                return 1;
                        }
                }
                //printf("Message sent\n");

                //Receive a reply from the server
                if( recv(sock , server_reply , RCV_BUFFER_SIZE , 0) < 0)
                {
                        printf("Recv failed. Error\n");
                }

                //Copy char ( source , destiny )
                copy_char ( server_reply , trackingrate_reply );


                //------------------------------------------------------------------
                //--------- Bloco de GET para Nome do Objeto -----------------------

                //Send get data
                for(i = 0; i<cont_vector_lines(get_object_name) ; i++)
                {
                        if( send(sock , get_object_name[i] , strlen(get_object_name[i]) , 0) < 0)
                        {
                                puts("Send failed. Error");
                                return 1;
                        }
                }
                //printf("Message sent\n");

                //Receive a reply from the server
                if( recv(sock , server_reply , RCV_BUFFER_SIZE , 0) < 0)
                {
                        printf("Recv failed. Error\n");
                }

                //Copy char ( source , destiny )
                copy_char ( server_reply , object_name_reply );


                //------------------------------------------------------------------
                //--------- Bloco de GET para Tempo Sideral do Objeto --------------

                //Send get data
                for(i = 0; i<cont_vector_lines(get_object_sidereal) ; i++)
                {
                        if( send(sock , get_object_sidereal[i] , strlen(get_object_sidereal[i]) , 0) < 0)
                        {
                                puts("Send failed. Error");
                                return 1;
                        }
                }
                //printf("Message sent\n");

                //Receive a reply from the server
                if( recv(sock , server_reply , RCV_BUFFER_SIZE , 0) < 0)
                {
                        printf("Recv failed. Error\n");
                }

                //Copy char ( source , destiny )
                copy_char ( server_reply , object_sidereal_reply );


                //------------------------------------------------------------------
                //--------- Bloco de GET para Julian Date do Objeto ----------------

                //Send get data
                for(i = 0; i<cont_vector_lines(get_object_jd) ; i++)
                {
                        if( send(sock , get_object_jd[i] , strlen(get_object_jd[i]) , 0) < 0)
                        {
                                puts("Send failed. Error");
                                return 1;
                        }
                }
                //printf("Message sent\n");

                //Receive a reply from the server
                if( recv(sock , server_reply , RCV_BUFFER_SIZE , 0) < 0)
                {
                        printf("Recv failed. Error\n");
                }

                //Copy char ( source , destiny )
                copy_char ( server_reply , object_jd_reply );







                //------------------------------------------------------------------
		//----------- Bloco para exibicao na tela --------------------------


		system("clear"); // Limpar a tela
		printf("Telescope Coordinates:\n");
		printf("-------------------------\n");
		puts(altaz_reply);
		printf("-------------------------\n");
		puts(radec_reply);
		printf("-------------------------\n");
		puts(trackingrate_reply);
		printf("-------------------------\n");
		puts(object_name_reply);
		printf("-------------------------\n");
                puts(object_sidereal_reply);
                printf("-------------------------\n");
                puts(object_jd_reply);
                printf("-------------------------\n");


		printf("\rLoop #%d\n", j);
		printf("\rLoading %c\n", spin[k]);
		fflush(stdout);
		j++;
		k++;
		if (k == 4) { k = 0; };
		close(sock);

	}

	printf("\n");
	return 0;
}

