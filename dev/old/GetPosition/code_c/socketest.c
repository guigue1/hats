/*
	C ECHO client example using sockets
*/
#include <stdio.h>	    //printf
#include <string.h>	    //strlen
#include <sys/socket.h>	//socket
#include <arpa/inet.h>	//inet_addr

int main(int argc , char *argv[])
{
	int i , sock;
	struct sockaddr_in server;
	char message[1000] , server_reply[2000];
    char* socket_message[] =
    {
        "/* Java Script */",
        "/* Socket Start Packet */",
        "var Out;",
        "Out='TheSky Build=' + Application.build",
        "/* Socket End Packet */"
    };

	//Create socket
	sock = socket(AF_INET , SOCK_STREAM , 0);
	if (sock == -1)
	{
		printf("Could not create socket");
	}
	puts("Socket created");

	server.sin_addr.s_addr = inet_addr("10.0.92.212");
	server.sin_family = AF_INET;
	server.sin_port = htons( 3040 );

	//Connect to remote server
	if (connect(sock , (struct sockaddr *)&server , sizeof(server)) < 0)
	{
		perror("connect failed. Error");
		return 1;
	}

	puts("Connected");

	//Send some data
	for(i = 0; socket_message[i] != '\0'; i++)
    {
        if( send(sock , socket_message[i] , strlen(socket_message[i]) , 0) < 0)
	    {
		    puts("Send failed");
		    return 1;
	    }
    }

	puts("Message sent\n");

	//Receive a reply from the server
	if( recv(sock , server_reply , 2000 , 0) < 0)
	{
		puts("recv failed");
	}

	puts("Server reply:");
	puts("-----------------------------------------------------");
	puts(server_reply);
	puts("-----------------------------------------------------");
	close(sock);
	return 0;
}
