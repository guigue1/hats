/*
	==============================================================
	Universidade Presbiteriana Mackenzie
	Centro de Rádio Astronomia e Astrofísica Mackenzie - CRAAM
	==============================================================

	GetPositionLoop_v1.0
	---------------------------------------------------
	Versão contendo todos os parametros selecionados para serem extraidos da
	montagem Paramount. Nesta versão os dados são apenas mostrados na tela.
	Utilizadas classes:
	sky6RASCOMTele e sky6ObjectInformation
	---------------------------------------------------

	Autor: Tiago Giorgetti
	Email: tiago.giorgetti@craam.mackenzie.br

	Histórico:
	_______________________________________________________________________________
	 Versão	|  Data		|	Atualização
	-------------------------------------------------------------------------------
	  0.1	|  06-10-2019	| Primeira versão.
	-------------------------------------------------------------------------------
	  1.0   |  31-10-2019   | Inclusão de informações do objeto e itens de tela
		|               | contadores de vetores e definicao da estrutura
	________|_______________|______________________________________________________

	Implementação do kbhit() na referencia baixo:
	https://www.raspberrypi.org/forums/viewtopic.php?t=188067 - acesso em 04-10-2019.

*/

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <termio.h>
#include <stdint.h>
#include <inttypes.h>

#include <semaphore.h>  //For Shared Memory
#include <sys/mman.h>   //For Shared Memory
#include <sys/stat.h>   //For Shared Memory

#include <sys/socket.h> //socket
#include <arpa/inet.h>  //inet_addr
#include <fcntl.h>      //open(sock) or file
#include <unistd.h>     //close(sock) or write to a file descriptor

#include <time.h> 	//usleep() to timing socket message transfer




//Parametros de conexao
#define IP_SERVER "10.0.92.12"
#define TCP_PORT 3040
#define RCV_BUFFER_SIZE 2000
#define TX_DELAY 0		//Microseconds


//Parametros para Husec_time()
#define SEC2HUSEC     10000L
#define HUSEC2NSEC   100000L
#define ONEDAY        86400L
#define HUSECS2HRS 36000000L
#define MIN2HUSEC    600000L
#define MIN              60L


//Parametros para Shared Memory
#define BackingFile "Hats_Ring_Buffer"
#define SemaphoreName "semaphore"
#define AccessPerms 0644


//Parametros do Ring Buffer
#define RINGSIZE 10






        /***
        *     P R E L I M I N A R Y    F U N C T I O N S
        *****************************************************************/




/*** Time Hundred of Micro Seconds (Husec) ***/

uint64_t husec_time(void)
{
	/****************************************************************************************

	HAXtime : Example program to get the time stamp in hundred of microseconds (husec)
          since 0:00:00 UTC using the C time library functions.

          Unix time ("Epoch") does not address correctly the leap seconds and,
          although for us is a minor problem (har to think we'll observe at 23:59:59)
          I do prefer to use our "traditional" SST solution: the husecs since
          the beginning of the observing day.

          clock_gettime(): gives the time in seconds since 01-01-1970 00:00:00 UTC

          To get the number of seconds since 00:00:00 we take the modulus (%)
          of the clock_gettime() output and the number of seconds in a day (86400
          in the uniform Unix time standard)

          Then we convert this number to husecs and add the nanoseconds (converted
          to husecs too).

          To check the results we convert the obtained husecs to hh:mm:ss, and compare
          with the time obatined by using time() function (For some reason, time()
          gives the time in BST and not in UTC).

          This solution will work only while the tv_sec (a signed long int) does not
          overflows. This should happen one day on 2038. Unless the library is
          corrected before.

          Guigue - 14-10-2019 T 18:58 BST

	******************************************************************************************/

	uint64_t all;
	time_t sec;
	struct timespec spec;

	clock_gettime(CLOCK_REALTIME, &spec);
//	now = time(NULL);

	/*  Convertion procedure  */
	sec = spec.tv_sec % ONEDAY;   	// get the number of seconds of the present day getting
               				// the remainder of the division by 86400 (total number of seconds in a day)

	all = (uint64_t) sec * SEC2HUSEC + (uint64_t) spec.tv_nsec / HUSEC2NSEC; // convert seconds to husecs
	                                                                         // convert nanoseconds of the present second to husecs
               		                                                         // and get the total

	/**************************************************************************

	// Printout the results
	printf("Current time: %" PRIu64  " Hundred of Microseconds since 0:00 \n", all);  // husecs of the day
	printf("Current time (UTC)        : %d:%0.2d:%0.2d\n",
        	all / HUSECS2HRS,
		(all / MIN2HUSEC) % MIN ,
		((all % HUSECS2HRS) % MIN2HUSEC) / SEC2HUSEC);  // husecs converted back to hour, min and seconds
	printf("Current time (BST = UTC-3): %s",ctime(&now));   // verification by using the internal time() function
                                                         	// time() is in local standard time (BST) !
	**************************************************************************/

	return all;
}



/* KeyBoard Hit */

bool kbhit(void)
{
	struct termios original;
	tcgetattr(STDIN_FILENO, &original);
	struct termios term;
	memcpy(&term, &original, sizeof(term));
	term.c_lflag &= ~ICANON;
	tcsetattr(STDIN_FILENO, TCSANOW, &term);
	int characters_buffered = 0;
	ioctl(STDIN_FILENO, FIONREAD, &characters_buffered);
	tcsetattr(STDIN_FILENO, TCSANOW, &original);
	bool pressed = (characters_buffered != 0);
	return pressed;
}



/* Move a string from a[] to b[] */

void copy_char ( char a[], char b[])
{
	int i = 0;
	do
	{
		b[i] = a[i];
		i++;
	}while ( a[i] != '|' );
}



/* Counter for char vector itens  */

int count_vector_lines ( char* a[])
{
	int i = 0;
	while ( a[i] != "!")
	{
		i++;
	}
	return i;
}



/* Message for Shared Memory */

void report_and_exit(const char* msg) {
  perror(msg);
  exit(-1);
}





        /***
        *     M A I N    F U N C T I O N
        *****************************************************************/




int main(int argc , char *argv[])
{

	struct pos_data_type
	{
		unsigned long long time_Husec 	;	//Time: Husec
		double time_JD			;	//Time: Julian Date
		double time_Sid			;	//Time: Sideral
		//--------------------------------------------------------
		double pos_tele_alt		;	//Telescope Position: Altitude
		double pos_tele_az		;	//Telescope Position: Azimute
		double pos_tele_ra		;	//Telescope Position: Right Ascension
		double pos_tele_dec		;	//Telescope Position: Declination
		//---------------------------------------------------------
		double rate_ObjId_ra		;	//Tracking rate: Right Ascension diff from sidereal rate
		double rate_ObjId_dec		;	//Tracking rate: Declination diff from sidereal rate

	} hats_pos_ringbuffer[RINGSIZE], *rb_ptr ;


	long int j = 0 							;	//For screen print
	int k = 0							;	//For screen print
	const char spin[4]={'|', '/', '-', '\\'}			;	//For screen print

	int i, sock							;	//Socket variable
	struct sockaddr_in server					;	//Socket variable
	char server_reply[RCV_BUFFER_SIZE]				;	//Generic variable of server reply

        char object_sidereal_reply[RCV_BUFFER_SIZE]			;	//Store of specific server reply
        char object_jd_reply[RCV_BUFFER_SIZE]				;       //Store of specific server reply
	char alt_reply[RCV_BUFFER_SIZE]					;       //Store of specific server reply
	char az_reply[RCV_BUFFER_SIZE]					;       //Store of specific server reply
	char ra_reply[RCV_BUFFER_SIZE]					;       //Store of specific server reply
	char dec_reply[RCV_BUFFER_SIZE]					;       //Store of specific server reply
	char RA_trackingrate_reply[RCV_BUFFER_SIZE]			;       //Store of specific server reply
	char DEC_trackingrate_reply[RCV_BUFFER_SIZE]			;       //Store of specific server reply

	static unsigned long long rb_ctr = 0				;	//Ring Buffer Counter
	rb_ptr = hats_pos_ringbuffer					;	//Ring Buffer Pointer (1st position)

	size_t ByteSize = sizeof(hats_pos_ringbuffer) * RINGSIZE	;	//Used for Shared Memory and to Write in file
	int fd_shmem							;	//Used for Shared Memory
	sem_t * semptr							;	//Used for Shared Memory

	int fd_data, fd_data_w;							//Used to Open and Write a File
	char *filename = "/home/tgiorgetti/HAX/HAX_DATA/getposition_file.bin";	//Used to Open/Create a File



	//----------Begin--Of--Javascripts--------------------------------------------

	char* get_ALT[] =
	{
		"/* Java Script */",
		"/* Socket Start Packet */",
		"var Out;",
		"sky6RASCOMTele.GetAzAlt();",
		"var alt = sky6RASCOMTele.dAlt;",
		"Out = alt",
		"/* Socket End Packet */",
		"!"	//End Caracter
	};

        char* get_AZ[] =
        {
                "/* Java Script */",
                "/* Socket Start Packet */",
                "var Out;",
                "sky6RASCOMTele.GetAzAlt();",
                "var az = sky6RASCOMTele.dAz;",
                "Out = az",
                "/* Socket End Packet */",
                "!"     //End Caracter
        };

	char* get_RA[] =
        {
                "/* Java Script */",
                "/* Socket Start Packet */",
                "var Out;",
                "sky6RASCOMTele.GetRaDec();",
                "var ra = sky6RASCOMTele.dRa;",
                "Out = ra",
                "/* Socket End Packet */",
		"!"	//End Caracter
        };

	char* get_DEC[] =
        {
                "/* Java Script */",
                "/* Socket Start Packet */",
                "var Out;",
                "sky6RASCOMTele.GetRaDec();",
                "var dec = sky6RASCOMTele.dDec;",
                "Out = dec",
                "/* Socket End Packet */",
                "!"     //End Caracter
        };

        char* get_RA_TrackingRate[] =
        {
                "/* Java Script */",
                "/* Socket Start Packet */",
                "var Out;",
                "sky6RASCOMTele.GetRaDec();",
                "var tra = sky6RASCOMTele.dRaTrackingRate;",
                "Out = tra",
                "/* Socket End Packet */",
		"!"	//End Caracter
        };

        char* get_DEC_TrackingRate[] =
        {
                "/* Java Script */",
                "/* Socket Start Packet */",
                "var Out;",
                "sky6RASCOMTele.GetRaDec();",
                "var tdec = sky6RASCOMTele.dDecTrackingRate;",
                "Out = tdec",
                "/* Socket End Packet */",
                "!"     //End Caracter
        };

        char* get_object_sidereal[] =
        {
                "/* Java Script */",
                "/* Socket Start Packet */",
                "var Out;",
                "sky6ObjectInformation.Property(173);",
                "var sidereal = sky6ObjectInformation.ObjInfoPropOut;",
                "Out = sidereal",
                "/* Socket End Packet */",
                "!"     //End Caracter
        };

        char* get_object_jd[] =
        {
                "/* Java Script */",
                "/* Socket Start Packet */",
                "var Out;",
                "sky6ObjectInformation.Property(174);",
                "var jd = sky6ObjectInformation.ObjInfoPropOut;",
                "Out = jd",
                "/* Socket End Packet */",
                "!"     //End Caracter
        };


	//----------End--Of--Javascripts--------------------------------------------






	/***** S H A R E D  M E M O R Y *****/

	fd_shmem = shm_open(BackingFile,      	/* name from smem.h */
        	O_RDWR | O_CREAT, 		/* read/write, create if needed */
		AccessPerms);   	  	/* access permissions (0644) */

	if (fd_shmem < 0) report_and_exit("Can't open shared mem segment...");

	ftruncate(fd_shmem, ByteSize); 		/* get the bytes */

	rb_ptr = mmap(NULL, 			/* let system pick where to put segment */
        	ByteSize,   			/* how many bytes */
                PROT_READ | PROT_WRITE,		/* access protections */
                MAP_SHARED, 			/* mapping visible to other processes */
                fd_shmem,      			/* file descriptor */
                0);         			/* offset: start at 1st byte */

	if ( (void *) -1  == rb_ptr)
	report_and_exit("Can't get segment...");
	else
	rb_ptr = hats_pos_ringbuffer;


	/**  Semaphore code to lock the shared mem  **/
	semptr = sem_open(SemaphoreName, 	/* name */
        	O_CREAT,       			/* create the semaphore */
		AccessPerms,   			/* protection perms */
 		0);            			/* initial value */

	if (semptr == (void*) -1) report_and_exit("sem_open");





        /***** F I L E  O P E N  *****/

	if ( (fd_data = open(filename, O_RDWR | O_CREAT | O_APPEND, AccessPerms)) == -1)
	{
		fprintf(stderr, "Cannot open getposition data file. Try again later.\n");
		exit(1);
	}







	/***
	*     I N F I N I T E    L O O P
	*****************************************************************/

	while(!kbhit())
	{

//FIRST	SOCKET	//Create socket 1
		sock = socket(AF_INET , SOCK_STREAM , 0);
		if (sock == -1)
		{
			printf("Could not create socket");
		}
		//printf("Socket created\n");

		server.sin_addr.s_addr = inet_addr(IP_SERVER);
        	server.sin_family = AF_INET;
		server.sin_port = htons(TCP_PORT);

		//Connect to remote server
		if (connect(sock , (struct sockaddr *)&server , sizeof(server)) < 0)
		{
			printf("Connect failed. Error\n");
			return 1;
		}
		//printf("Connected\n");


		//------------------------------------------------------------------
		//------------ Altitude Coordinate GET Block Code ------------------

		// Send get data
		for(i = 0; i<count_vector_lines(get_ALT) ; i++)
		{
			if( send(sock , get_ALT[i] , strlen(get_ALT[i]) , 0) < 0)
			{
				puts("Send failed. Error");
				return 1;
			}
			usleep(TX_DELAY);
		}
		//printf("Message sent\n");

		//Receive a reply from the server
		if( recv(sock , server_reply , RCV_BUFFER_SIZE , 0) < 0)
		{
			printf("Recv failed. Error\n");
		}

		//Copy char ( source , destiny )
		copy_char ( server_reply , alt_reply );


                //------------------------------------------------------------------
                //------------- Azimute Coordinate GET Block Code ------------------

                // Send get data
                for(i = 0; i<count_vector_lines(get_AZ) ; i++)
                {
                        if( send(sock , get_AZ[i] , strlen(get_AZ[i]) , 0) < 0)
                        {
                                puts("Send failed. Error");
                                return 1;
                        }
			usleep(TX_DELAY);
                }
                //printf("Message sent\n");

                //Receive a reply from the server
                if( recv(sock , server_reply , RCV_BUFFER_SIZE , 0) < 0)
                {
                        printf("Recv failed. Error\n");
                }

                //Copy char ( source , destiny )
                copy_char ( server_reply , az_reply );


                //------------------------------------------------------------------
                //----------- Right Ascension (RA) Coordinate GET Block Code -------

		//Send get data
                for(i = 0; i<count_vector_lines(get_RA) ; i++)
                {
                        if( send(sock , get_RA[i] , strlen(get_RA[i]) , 0) < 0)
                        {
                                puts("Send failed. Error");
                                return 1;
                        }
			usleep(TX_DELAY);
                }
		//printf("Message sent\n");

                //Receive a reply from the server
                if( recv(sock , server_reply , RCV_BUFFER_SIZE , 0) < 0)
                {
                        printf("Recv failed. Error\n");
                }

		//Copy char ( source , destiny )
                copy_char ( server_reply , ra_reply );


                //------------------------------------------------------------------
                //---------- Declination (DEC) Coordinate GET Block Code -----------

                //Send get data
                for(i = 0; i<count_vector_lines(get_DEC) ; i++)
                {
                        if( send(sock , get_DEC[i] , strlen(get_DEC[i]) , 0) < 0)
                        {
                                puts("Send failed. Error");
                                return 1;
                        }
			usleep(TX_DELAY);
                }
                //printf("Message sent\n");

                //Receive a reply from the server
                if( recv(sock , server_reply , RCV_BUFFER_SIZE , 0) < 0)
                {
                        printf("Recv failed. Error\n");
                }

                //Copy char ( source , destiny )
                copy_char ( server_reply , dec_reply );


                //------------------------------------------------------------------
                //--------- Tracking Rate (RA) GET Block Code ----------------------

                //Send get data
                for(i = 0; i<count_vector_lines(get_RA_TrackingRate) ; i++)
                {
                        if( send(sock , get_RA_TrackingRate[i] , strlen(get_RA_TrackingRate[i]) , 0) < 0)
                        {
                                puts("Send failed. Error");
                                return 1;
                        }
			usleep(TX_DELAY);
                }
                //printf("Message sent\n");

                //Receive a reply from the server
                if( recv(sock , server_reply , RCV_BUFFER_SIZE , 0) < 0)
                {
                        printf("Recv failed. Error\n");
                }

                //Copy char ( source , destiny )
                copy_char ( server_reply , RA_trackingrate_reply );



                //=================================================================
                close(sock);
                //=================================================================


//SECOND SOCKET  //Create socket 2
                sock = socket(AF_INET , SOCK_STREAM , 0);
                if (sock == -1)
                {
                        printf("Could not create socket");
                }
                //printf("Socket created\n");

                server.sin_addr.s_addr = inet_addr(IP_SERVER);
                server.sin_family = AF_INET;
                server.sin_port = htons(TCP_PORT);

                //Connect to remote server
                if (connect(sock , (struct sockaddr *)&server , sizeof(server)) < 0)
                {
                        printf("Connect failed. Error\n");
                        return 1;
                }
                //printf("Connected\n");


                //------------------------------------------------------------------
                //--------- Tracking Rate (DEC) GET Block Code ---------------------

                //Send get data
                for(i = 0; i<count_vector_lines(get_DEC_TrackingRate) ; i++)
                {
                        if( send(sock , get_DEC_TrackingRate[i] , strlen(get_DEC_TrackingRate[i]) , 0) < 0)
                        {
                                puts("Send failed. Error");
                                return 1;
                        }
			usleep(TX_DELAY);
                }
                //printf("Message sent\n");

                //Receive a reply from the server
                if( recv(sock , server_reply , RCV_BUFFER_SIZE , 0) < 0)
                {
                        printf("Recv failed. Error\n");
                }

                //Copy char ( source , destiny )
                copy_char ( server_reply , DEC_trackingrate_reply );


                //------------------------------------------------------------------
                //------------ Sidereal Time Object GET Block Code -----------------

                //Send get data
                for(i = 0; i<count_vector_lines(get_object_sidereal) ; i++)
                {
                        if( send(sock , get_object_sidereal[i] , strlen(get_object_sidereal[i]) , 0) < 0)
                        {
                                puts("Send failed. Error");
                                return 1;
                        }
			usleep(TX_DELAY);
                }
                //printf("Message sent\n");

                //Receive a reply from the server
                if( recv(sock , server_reply , RCV_BUFFER_SIZE , 0) < 0)
                {
                        printf("Recv failed. Error\n");
                }

                //Copy char ( source , destiny )
                copy_char ( server_reply , object_sidereal_reply );


                //------------------------------------------------------------------
                //------------- Julian Date Object GET Block Code ------------------

                //Send get data
                for(i = 0; i<count_vector_lines(get_object_jd) ; i++)
                {
                        if( send(sock , get_object_jd[i] , strlen(get_object_jd[i]) , 0) < 0)
                        {
                                puts("Send failed. Error");
                                return 1;
                        }
			usleep(TX_DELAY);
                }
                //printf("Message sent\n");

                //Receive a reply from the server
                if( recv(sock , server_reply , RCV_BUFFER_SIZE , 0) < 0)
                {
                        printf("Recv failed. Error\n");
                }

                //Copy char ( source , destiny )
                copy_char ( server_reply , object_jd_reply );


		//=================================================================
		close(sock);
		//=================================================================




		//------------------------------------------------------------------
                //------------- Ring Buffer Storage Block Code ---------------------

              	rb_ptr->time_Husec       = husec_time()                  ;
              	rb_ptr->time_JD          = atof (object_jd_reply)        ;
              	rb_ptr->time_Sid         = atof (object_sidereal_reply)  ;
              	rb_ptr->pos_tele_alt     = atof (alt_reply)              ;
              	rb_ptr->pos_tele_az      = atof (az_reply)               ;
              	rb_ptr->pos_tele_ra      = atof (ra_reply)               ;
              	rb_ptr->pos_tele_dec     = atof (dec_reply)              ;
              	rb_ptr->rate_ObjId_ra    = atof (RA_trackingrate_reply)  ;
              	rb_ptr->rate_ObjId_dec   = atof (DEC_trackingrate_reply) ;


                //------------------------------------------------------------------
                //------ Increment the semaphore so that memreader can read  -------

		if (sem_post(semptr) < 0) report_and_exit("sem_post");


		if ((rb_ctr%RINGSIZE) == 0 && rb_ctr != 0  )
                {
			if ( (fd_data_w = write(fd_data , hats_pos_ringbuffer , sizeof(hats_pos_ringbuffer))) < 0)
                	{
                        	perror("Problem in writing to file");
                        	exit(1);
                	}
		}




                //------------------------------------------------------------------
		//----------- Screen Prints Block Code -----------------------------

		system("clear"); // Clean Screen
		printf("Telescope Coordinates:\n");
		printf("-------------------------\n");
                puts(object_jd_reply);
                printf("-------------------------\n");
                puts(object_sidereal_reply);
                printf("-------------------------\n");
		puts(alt_reply);
		printf("-------------------------\n");
		puts(az_reply);
                printf("-------------------------\n");
		puts(ra_reply);
		printf("-------------------------\n");
                puts(dec_reply);
                printf("-------------------------\n");
		puts(RA_trackingrate_reply);
		printf("-------------------------\n");
                puts(DEC_trackingrate_reply);
                printf("\n-------------------------\n");
                printf("-------------------------\n");
                printf("-------------------------\n\n");
		printf("%Lu\n",rb_ptr->time_Husec);
                printf("-------------------------\n");
		printf("%lf\n",rb_ptr->time_JD);
                printf("-------------------------\n");
		printf("%lf\n",rb_ptr->time_Sid);
                printf("-------------------------\n");
		printf("%lf\n",rb_ptr->pos_tele_alt);
                printf("-------------------------\n");
		printf("%lf\n",rb_ptr->pos_tele_az);
                printf("-------------------------\n");
		printf("%lf\n",rb_ptr->pos_tele_ra);
                printf("-------------------------\n");
		printf("%lf\n",rb_ptr->pos_tele_dec);
                printf("-------------------------\n");
		printf("%lf\n",rb_ptr->rate_ObjId_ra);
                printf("-------------------------\n");
		printf("%lf\n",rb_ptr->rate_ObjId_dec);
                printf("-------------------------\n\n\n");

		printf("\r Loading %c\n", spin[k]);
		printf("\r Loop #%ld\n\n", j);
		fflush(stdout);
		j++;
		k++;
		if (k == 4) { k = 0; };

		printf("\r New Position       = %llu\n",rb_ctr%RINGSIZE);
		printf("\r Address Position   = %lu\n",rb_ptr);
		printf("\r RingBuffer Counter = %Lu\n\n\n", rb_ctr);

		fprintf(stderr, "\r Shared mem address: %p [0..%lu]\n", rb_ptr, ByteSize - 1);
		fprintf(stderr, "\r Backing file:       /dev/shm/%s\n", BackingFile );




                //------------------------------------------------------------------
                //----------- RING BUFFER POINTER COUNTER --------------------------

		rb_ctr++;
		if (rb_ctr%RINGSIZE)
		{rb_ptr++;} else { rb_ptr = hats_pos_ringbuffer;};


		//------------------------------------------------------------------
                //----------- L O O P   E N D --------------------------------------


	}
	close(fd_data);
	printf("\n");
	return 0;
}

