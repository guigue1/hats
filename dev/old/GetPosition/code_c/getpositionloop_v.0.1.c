/*
	==============================================================
	Universidade Presbiteriana Mackenzie
	Centro de Rádio Astronomia e Astrofísica Mackenzie - CRAAM
	==============================================================

	GetPositionLoop_v0.1
	---------------------------------------------------
	Promove um loop infinito enquanto uma tecla não é pressionada, via kbhit(),
	e retorna do servidor socket do TheSkyX o valor da coordenada AltAz
	(Altitude+Azimute).
	Função javascript utilizada: sky6RASCOMTele.GetAzAlt()
	Código C é uma sub rotina da switch HAX.
	---------------------------------------------------

	Autor: Tiago Giorgetti
	Email: tiago.giorgetti@craam.mackenzie.br

	Histórico:
	_______________________________________________________________________________
	 Versão	|  Data		|	Atualização
	-------------------------------------------------------------------------------
	  0.1	|  06-10-2019	| Primeira versão.
	-------------------------------------------------------------------------------
	________|_______________|______________________________________________________

	Implementação do kbhit() na referencia baixo:
	https://www.raspberrypi.org/forums/viewtopic.php?t=188067 - acesso em 04-10-2019.

*/

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <termio.h>
#include <unistd.h>

#include <sys/socket.h> //socket
#include <arpa/inet.h>  //inet_addr
#include <fcntl.h>      //open(sock)
#include <unistd.h>     //close(sock)



bool kbhit(void)
{
	struct termios original;
	tcgetattr(STDIN_FILENO, &original);
	struct termios term;
	memcpy(&term, &original, sizeof(term));
	term.c_lflag &= ~ICANON;
	tcsetattr(STDIN_FILENO, TCSANOW, &term);
	int characters_buffered = 0;
	ioctl(STDIN_FILENO, FIONREAD, &characters_buffered);
	tcsetattr(STDIN_FILENO, TCSANOW, &original);
	bool pressed = (characters_buffered != 0);
	return pressed;
}

int main(int argc , char *argv[])
{
	long int j = 0;
	system("clear");

	int i , sock;
	struct sockaddr_in server;
	char message[1000] , server_reply[2000];
	char* socket_message[] =
	{
		"/* Java Script */",
		"/* Socket Start Packet */",
		"var Out;",
		"sky6RASCOMTele.GetAzAlt();",
		"var az = sky6RASCOMTele.dAz;",
		"var alt = sky6RASCOMTele.dAlt;",
		"Out = 'Az:' + az + ' ' + 'Alt=' + alt",
		"/* Socket End Packet */"
	};

	while(!kbhit())
	{

		//Create socket
		sock = socket(AF_INET , SOCK_STREAM , 0);
		if (sock == -1)
		{
			printf("Could not create socket");
		}
//		printf("Socket created\n");

		server.sin_addr.s_addr = inet_addr("10.0.92.212");
        	server.sin_family = AF_INET;
		server.sin_port = htons( 3040 );

		//Connect to remote server
		if (connect(sock , (struct sockaddr *)&server , sizeof(server)) < 0)
		{
			perror("Connect failed. Error");
			return 1;
		}
//		printf("Connected\n");

		//Send some data
		for(i = 0; i<8 ; i++)
		{
			if( send(sock , socket_message[i] , strlen(socket_message[i]) , 0) < 0)
			{
				puts("Send failed. Error");
				return 1;
			}
		}
//		printf("Message sent\n");

		//Receive a reply from the server
		if( recv(sock , server_reply , 2000  , 0) < 0)
		{
			printf("Recv failed. Error\n");
		}

		system("clear"); // Limpar a tela
		printf("Server reply:\n");
		printf("------------------------------------------------------------------\n");
		puts(server_reply);
		printf("------------------------------------------------------------------\n");

		printf("\rLoop #%d\n", j);
//              system("sleep 1");
                fflush(stdout);
		j++;
		close(sock);

	}

	printf("\n");
	return 0;
}
