/*
        ===============================================================================
        Universidade Presbiteriana Mackenzie
        Centro de Rádio Astronomia e Astrofísica Mackenzie - CRAAM
        ===============================================================================

        GetPosition_v0.1
        -------------------------------------------------------------------------------
        Promove um loop infinito enquanto uma tecla não é pressionada, via kbhit(),
        e retorna do servidor socket do TheSkyX o valor da coordenada AltAz
        (Altitude+Azimute).
        Função javascript utilizada: sky6RASCOMTele.GetAzAlt()
        Código C é uma sub rotina da switch HAX.
        -------------------------------------------------------------------------------

        Autor: Tiago Giorgetti
        Email: tiago.giorgetti@craam.mackenzie.br

        Histórico:
        _______________________________________________________________________________
         Versão |  Data         |       Atualização
        -------------------------------------------------------------------------------
          0.1   |  06-10-2019   | Primeira versão.
        -------------------------------------------------------------------------------
        ________|_______________|______________________________________________________

        Implementação do kbhit() na referencia baixo:
        https://www.raspberrypi.org/forums/viewtopic.php?t=188067 - acesso em 04-10-2019.

*/

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <termio.h>
#include <unistd.h>

#include <sys/socket.h> //socket
#include <arpa/inet.h>  //inet_addr
#include <fcntl.h>      //open(sock)
#include <unistd.h>     //close(sock)



//Parametros de conexao
#define IP_SERVER "10.0.92.12"
#define TCP_PORT 3040
#define RCV_BUFFER_SIZE 2000


bool kbhit(void)
{
        struct termios original;
        tcgetattr(STDIN_FILENO, &original);
        struct termios term;
        memcpy(&term, &original, sizeof(term));
        term.c_lflag &= ~ICANON;
        tcsetattr(STDIN_FILENO, TCSANOW, &term);
        int characters_buffered = 0;
        ioctl(STDIN_FILENO, FIONREAD, &characters_buffered);
        tcsetattr(STDIN_FILENO, TCSANOW, &original);
        bool pressed = (characters_buffered != 0);
        return pressed;
}

